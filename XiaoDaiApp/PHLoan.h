//
//  PHLoan.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PHLoan : MTLModel<MTLJSONSerializing>
@property (nonatomic, readonly, copy) NSNumber *amount;
@property (nonatomic, readonly, copy) NSNumber *duration;
@property (nonatomic, readonly, copy) NSNumber *valuation;
@property (nonatomic, readonly, copy) NSString *applicantName;
@property (nonatomic, readonly, copy) NSString *status;

@property (nonatomic, readonly, copy) NSString *applicantPhone;
@property (nonatomic, readonly, copy) NSString *applicantIdentificationNumber;
@property (nonatomic, readonly, copy) NSString *agentName;
@property (nonatomic, readonly, copy) NSString *agentPhone;
@property (nonatomic, readonly, copy) NSString *agentIdentificationNumber;
@property (nonatomic, readonly, copy) NSString *address;
@property (nonatomic, readonly, copy) NSString *type;

@property (nonatomic, readonly, copy) NSNumber *buildArea;
@property (nonatomic, readonly, copy) NSNumber *unitPrice;
@property (nonatomic, readonly, copy) NSNumber *avagePrice;
@property (nonatomic, readonly, copy) NSNumber *maxPrice;
@property (nonatomic, readonly, copy) NSNumber *minPrice;

@property (nonatomic, readonly, copy) NSNumber *appId;

@end
