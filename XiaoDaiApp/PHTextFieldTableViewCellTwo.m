//
//  PHTextFieldTableViewCellTwo.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTextFieldTableViewCellTwo.h"
#import "UITextField+phc_addition.h"
#import "UILabel+phc_addition.h"
#import <Masonry/Masonry.h>

@interface PHTextFieldTableViewCellTwo()<UITextFieldDelegate>

@property (nonatomic, strong, readwrite) UITextField *textField;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic,assign) BOOL idNumTextField;

@end

@implementation PHTextFieldTableViewCellTwo

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [UILabel phc_labelWithFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
        _titleLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_titleLabel];
        
        _textField = [UITextField phc_textFieldWithFontSize:15.0f textColor:[UIColor darkGrayColor] placeholderText:nil text:nil];
        _textField.delegate = self;
        [self.contentView addSubview:_textField];
        
        _rightLabel = [UILabel phc_labelWithFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
        _rightLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_rightLabel];
        
        typeof(self) __weak weakSelf = self;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.contentView.mas_width).with.multipliedBy(0.3f);
        }];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.width.equalTo(weakSelf.contentView.mas_width).with.multipliedBy(0.35f);
            make.left.equalTo(_titleLabel.mas_right).with.offset(25.0f);
        }];
        
        [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.left.equalTo(weakSelf.textField.mas_right);
            make.width.equalTo(weakSelf.contentView.mas_width).with.multipliedBy(0.2f);
        }];
    }
    
    return self;
}

- (void)configureWithTitle:(NSString *)title placeholder:(NSString *)placeholder text:(NSString *)text rightText:(NSString *)rightText{
    self.titleLabel.text = title;
    self.textField.placeholder = placeholder;
    self.textField.text = nil;
    self.rightLabel.text = rightText;
}
-(void)setTitleTextColor:(UIColor *)color{
    self.titleLabel.textColor = color;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
