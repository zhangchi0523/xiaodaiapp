//
//  PHReferralQRCodeViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/10.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHReferralQRCodeViewController.h"
#import "PHSessionManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UILabel+phc_addition.h"
#import "PHUserProfile.h"
#import "UIColor+phc_addition.h"
@interface PHReferralQRCodeViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) UILabel *label;

@end

@implementation PHReferralQRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的二维码";
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://10.99.1.119:9000/users/referralqrcode"]];
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableRequest addValue:@"pastman" forHTTPHeaderField:@"X-Requested-With"];
    [mutableRequest addValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    [self.view addSubview: webView];
    [webView loadRequest:mutableRequest];
    webView.delegate = self;
    PHSessionManager *sessionManager = [[PHSessionManager alloc]init];
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    NSInteger screenWidth = screenFrame.size.width;
    NSInteger screenHeight = screenFrame.size.height;
    _label = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:2];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"你好%@,邀请业务员请用以上二维码, 或者分享链接",sessionManager.currentUserProfile.name]];
    [attriString addAttribute:(NSString *)NSForegroundColorAttributeName value:[UIColor phc_blue] range:NSMakeRange(2,sessionManager.currentUserProfile.name.length)];

    self.label.attributedText = attriString;
    self.label.frame = CGRectMake(screenWidth*0.1f, screenHeight*0.6f, screenWidth*0.8f, 40);
    [[UIApplication sharedApplication].keyWindow addSubview:self.label];
    self.label.hidden = YES;

    // Do any additional setup after loading the view.
}
- (void )webViewDidStartLoad:(UIWebView  *)webView {
    _hud =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void )webViewDidFinishLoad:(UIWebView  *)webView{
    [self.hud hideAnimated:YES];
    self.label.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    if (self.label) {
        self.label.hidden = YES;

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
