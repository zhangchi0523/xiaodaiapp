//
//  PHTextFieldTableViewCellTwo.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewCell.h"
#import <UIKit/UIKit.h>

@interface PHTextFieldTableViewCellTwo : PHTableViewCell

@property (nonatomic, strong, readonly) UITextField *textField;

- (void)configureWithTitle:(NSString *)title placeholder:(NSString *)placeholder text:(NSString *)text rightText:(NSString *)rightText;
-(void)setTitleTextColor:(UIColor *)color;

@end
