//
//  PHMessage.m
//  p2p
//
//  Created by Philip on 9/15/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHMessage.h"

@implementation PHMessage

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"message":@"messageContent",
             @"date":@"pushTime",
             @"ID":@"id",
             @"type":@"pushType",
             @"isread":@"isread",
             @"borrowId":@"ext.borrowId",
             @"detailFlag": @"ext.detailFlag",
             };
}

//#define DATE_STRING

#ifdef DATE_STRING

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}


+ (NSValueTransformer *)dateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}


#else

+ (NSValueTransformer *)dateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSNumber *date, BOOL *success, NSError **error){
        return [NSDate dateWithTimeIntervalSince1970:[date floatValue]];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [NSNumber numberWithDouble:date.timeIntervalSince1970];
    }];

}

#endif


+ (NSValueTransformer *)typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
                                                                           @1: @(PHSystemMessage),
                                                                           @2: @(PHCreditMessage),
                                                                           @3: @(PHLoanMessage)
                                                                           }];
}

@end
