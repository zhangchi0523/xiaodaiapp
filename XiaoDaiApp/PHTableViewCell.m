//
//  PHTableViewCell.m
//  yqrx
//
//  Created by Philip on 11/1/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTableViewCell.h"

@implementation PHTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.separatorInset = UIEdgeInsetsMake(0.0f, self.bounds.size.width, 0.0f, self.bounds.size.width);
}

@end
