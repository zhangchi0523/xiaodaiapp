//
//  UIFont+phc_addition.m
//  p2p
//
//  Created by Philip on 9/6/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIFont+phc_addition.h"

@implementation UIFont (phc_addition)

+ (UIFont *)phc_defaultFontWithSize:(CGFloat)size {
    return [self fontWithName:@"HelveticaNeue-Light" size:size];
}

+ (UIFont *)phc_defaultBoldFontWithSize:(CGFloat)size {
    return [self fontWithName:@"HelveticaNeue-Medium" size:size];
}

@end
