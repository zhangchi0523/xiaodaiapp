//
//  UIViewController+phc_addition.m
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIViewController+phc_addition.h"
#import <Bolts/Bolts.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+phc_addition.h"
#import <AliyunOSSiOS/OSSService.h>
#import <AFNetworking/AFNetworking.h>
#import "PHPickerViewController.h"
#import "PHSessionManager.h"
#import "PHUserProfile.h"
NSString * const PHImageName = @"selfPhoto.jpg";

NSString * const PHNotificationUploadPicture = @"PHNotificationUploadPicture";

@interface UIViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@end

@implementation UIViewController (phc_addition)

- (BFTask *)taskToShowPickerDataArray:(NSArray *)array {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    UIViewController *controller = self;
    
    if (([self isKindOfClass:UITabBarController.class])) {
        UINavigationController *nav = [(UITabBarController *)self selectedViewController];
        controller = nav.topViewController;
    }
    
    PHPickerViewController *shareViewController = [[PHPickerViewController alloc] initWithContentArray:array DismissHandler:^(PHPickerViewController *controller) {
        [controller willMoveToParentViewController:nil];
        [controller removeFromParentViewController];
        [controller.view removeFromSuperview];
        self.tabBarController.tabBar.hidden=NO;
        [completionSource setResult:controller.dataString];
    }];
    
    [controller addChildViewController:shareViewController];
    [controller.view addSubview:shareViewController.view];
    shareViewController.view.frame = controller.view.bounds;
    [shareViewController didMoveToParentViewController:controller];
    
    return completionSource.task;
}

- (BFTask *)taskToShowError:(NSError *)error {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    NSData *responseData = (NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:[dic objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    return completionSource.task;
}

- (BFTask *)taskToShowPrompt:(NSString *)prompt {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:prompt preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    return completionSource.task;
}

- (BFTask *)taskToShowDetermineOrCancelPrompt:(NSString *)prompt {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:prompt preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@YES];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@NO];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    return completionSource.task;
}
-(void)choseAndTakePicture{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = NO;
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePicker animated:YES completion:nil];
        } else {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    UIAlertAction *pictureAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:photoAction];
    [alertController addAction:pictureAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *editedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [editedImage phc_imageToReszieWithsize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height) compressionQuality:0.5f];
       
        NSDictionary *dic = @{@"image":image};
        [[NSNotificationCenter defaultCenter]postNotificationName:PHNotificationUploadPicture object:nil userInfo:dic];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (BFTask *)taskToUploadPictureToAliOSSWithData:(NSArray *)dataArray withNameArray:(NSArray *)nameArray{
    typeof(self) __weak weakSelf = self;
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSString *endpoint = [NSString stringWithFormat:@"http://%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"]];
    id<OSSCredentialProvider>credential = [[OSSPlainTextAKSKPairCredentialProvider alloc]initWithPlainTextAccessKey:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"OSSAccessKey"] secretKey:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"OSSSecretKey"]];
    OSSClient *client = [[OSSClient alloc]initWithEndpoint:endpoint credentialProvider:credential];
    NSMutableArray *uploadCountArray = [NSMutableArray new];
    
    for (NSInteger i=0;i<dataArray.count;i++) {
        OSSPutObjectRequest *put = [OSSPutObjectRequest new];
        put.bucketName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"];
        put.objectKey = [nameArray objectAtIndex:i];
        put.uploadingData = [dataArray objectAtIndex:i];
        OSSTask *putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task){
            
            if (!task.error) {
                [uploadCountArray addObject:task.result];
                NSLog(@"upload object success!");
                if (uploadCountArray.count == dataArray.count) {
                    [completionSource setResult:@YES];
                    [uploadCountArray removeAllObjects];
                    return task;
                }
            } else {
                [weakSelf taskToShowError:task.error];
            }
            return nil;
        }];
        
    }
    return completionSource.task;
}
- (NSString *)headerImagePath:(NSString *)imageName
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    PHSessionManager *sessionManager = [[PHSessionManager alloc]init];
    NSString* pathToFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", sessionManager.currentUserProfile.phone, imageName]];
    
    return pathToFile;
}
@end
