//
//  PHTextFieldTableViewCell.h
//  p2p
//
//  Created by Philip on 10/21/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTableViewCell.h"

@interface PHTextFieldTableViewCell : PHTableViewCell

@property (nonatomic, strong, readonly) UITextField *textField;

- (void)configureWithTitle:(NSString *)title placeholder:(NSString *)placeholder text:(NSString *)text;
- (void)configureWithKeyBoardType:(UIKeyboardType)keyBoardType withIdNumTextField:(BOOL)IdNumTextField;

@end
