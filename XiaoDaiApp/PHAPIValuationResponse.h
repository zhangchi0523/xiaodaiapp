//
//  PHAPIValuationResponse.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIBaseResponse.h"
@class PHValuationModel;

@interface PHAPIValuationResponse : PHAPIBaseResponse
@property (nonatomic, strong, readonly) PHValuationModel *AvagePrice;

@end
