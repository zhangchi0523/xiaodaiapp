//
//  PHUpdatePictureViewController.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/16.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHViewController.h"
#import "PHApplications.h"

@interface PHUpdatePictureViewController : PHViewController
- (instancetype)initWithName:(NSString*)name withPhone:(NSString *)phone withIdNumber:(NSString *)idNum withType:(NSString *)type withAccount:(NSString *)account withValuationId:(NSString *)valuationId withAddress:(NSString *)address withTerm:(NSString *)term;
@end
