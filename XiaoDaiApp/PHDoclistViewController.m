//
//  PHDoclistViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/10.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHDoclistViewController.h"
#import "PHSessionManager.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface PHDoclistViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation PHDoclistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预约资料清单";
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://10.99.1.119:9000/tools/doclist"]];
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableRequest addValue:@"pastman" forHTTPHeaderField:@"X-Requested-With"];
    [mutableRequest addValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    [self.view addSubview: webView];
    [webView loadRequest:mutableRequest];
    webView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void )webViewDidStartLoad:(UIWebView  *)webView {
    _hud =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void )webViewDidFinishLoad:(UIWebView  *)webView{
    [self.hud hideAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
