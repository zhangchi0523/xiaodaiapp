//
//  UIButton+phc_addition.m
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIButton+phc_addition.h"
#import "UIFont+phc_addition.h"
#import "UIColor+phc_addition.h"
#import "UIImage+phc_addition.h"

@implementation UIButton (phc_addition)

+ (instancetype)phc_buttonWithFontSize:(CGFloat)size title:(NSString *)title textColor:(UIColor *)color backgroundImage:(UIImage *)backgroundImage selectedBackgroundImage:(UIImage *)selectedBackgroundImage {
    UIButton *button = [self.class new];
    
    button.titleLabel.font = [UIFont phc_defaultFontWithSize:size];
    [button setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [button setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [button setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [button setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [button setContentEdgeInsets:UIEdgeInsetsMake(5.0f, 15.0f, 5.0f, 15.0f)];
    [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [button setBackgroundImage:selectedBackgroundImage forState:UIControlStateSelected];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    
    return button;
}

+ (instancetype)phc_buttonWithFontSize:(CGFloat)size title:(NSString *)title textColor:(UIColor *)color selectedTextColor:(UIColor *)selectedColor titleEdgeInsets:(UIEdgeInsets)insets {
    UIButton *button = [self.class new];
    
    button.titleLabel.font = [UIFont phc_defaultFontWithSize:size];
    [button setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [button setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [button setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [button setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [button setContentEdgeInsets:insets];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    [button setTitleColor:selectedColor forState:UIControlStateSelected];
    
    return button;
}

+ (instancetype)phc_actionButtonWithTitle:(NSString *)title {
    UIButton *button = [self phc_buttonWithFontSize:16.0f title:title textColor:[UIColor whiteColor] backgroundImage:[UIImage phc_imageWithColor:UIColorFromRGB(0x4A90E2)] selectedBackgroundImage:[UIImage phc_imageWithColor:UIColorFromRGB(0x00448b)]];
    [button setBackgroundImage:[UIImage phc_imageWithColor:UIColorFromRGB(0x4A90E2)] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[UIImage phc_imageWithColor:UIColorFromRGB(0x666666)] forState:UIControlStateDisabled];
    button.layer.cornerRadius = 5.0f;
    button.clipsToBounds = YES;
    button.contentEdgeInsets = UIEdgeInsetsMake(5.0f, 10.0f, 5.0f, 10.0f);
    return button;
}

@end
