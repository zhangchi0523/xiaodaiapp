//
//  PHAPIGetLoanListResponse.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIBaseResponse.h"
#import "PHLoan.h"
@interface PHAPIGetLoanListResponse : PHAPIBaseResponse
@property (nonatomic, strong, readonly) NSArray *loanList;
@property (nonatomic, strong, readonly) NSNumber *total;
@property (nonatomic, strong, readonly) NSNumber *page;

@end
