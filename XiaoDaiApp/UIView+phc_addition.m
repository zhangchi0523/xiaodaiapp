//
//  UIView+phc_addition.m
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIView+phc_addition.h"
#import <Masonry/Masonry.h>

static const NSUInteger kBorderViewTag = 12345;

@implementation UIView (phc_addition)

- (UIView *)phc_configureBottomBorderWithHeight:(CGFloat)height color:(UIColor *)color {
    UIView *borderView = [UIView new];
    borderView.tag = kBorderViewTag;
    borderView.backgroundColor = color;
    
    [self addSubview:borderView];
    
    typeof(self) __weak weakSelf = self;
    [borderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.width.equalTo(weakSelf.mas_width);
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.height.equalTo(@(height));
    }];
    
    return borderView;
}

- (UIView *)phc_configureTopBorderWithHeight:(CGFloat)height color:(UIColor *)color {
    UIView *borderView = [UIView new];
    borderView.tag = kBorderViewTag;
    borderView.backgroundColor = color;
    
    [self addSubview:borderView];
    
    typeof(self) __weak weakSelf = self;
    [borderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(weakSelf.mas_width);
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.height.equalTo(@(height));
    }];
    
    return borderView;
}

- (UIView *)phc_configureLeftBorderWithWidth:(CGFloat)width color:(UIColor *)color {
    UIView *borderView = [UIView new];
    borderView.tag = kBorderViewTag;
    borderView.backgroundColor = color;
    
    [self addSubview:borderView];
    
    typeof(self) __weak weakSelf = self;
    [borderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.height.equalTo(weakSelf.mas_height);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(@(width));
    }];
    
    return borderView;
}

- (UIView *)phc_configureRightBorderWithWidth:(CGFloat)width color:(UIColor *)color {
    UIView *borderView = [UIView new];
    borderView.tag = kBorderViewTag;
    borderView.backgroundColor = color;
    
    [self addSubview:borderView];
    
    typeof(self) __weak weakSelf = self;
    [borderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(weakSelf.mas_height);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(@(width));
    }];
    
    return borderView;
}

- (void)phc_setBorderColor:(UIColor *)color {
    UIView *borderView = [self viewWithTag:kBorderViewTag];
    borderView.backgroundColor = color;
}

@end
