//
//  PHLoanlistTableViewCell.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLoanlistTableViewCell.h"
#import "UILabel+phc_addition.h"
#import <Masonry.h>
#import "PHLoan+phc_string.h"
@interface PHLoanlistTableViewCell()
@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) UILabel *durationLabel;
@property (nonatomic, strong) UILabel *valuationLabel;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *statusLabel;

@end
@implementation PHLoanlistTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _amountLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:1];
        _amountLabel.adjustsFontSizeToFitWidth = YES;
        _amountLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_amountLabel];
        
        _durationLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:1];
        _durationLabel.textAlignment = NSTextAlignmentCenter;
        _durationLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_durationLabel];
        _valuationLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:1];
        _valuationLabel.textAlignment = NSTextAlignmentCenter;
        _valuationLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_valuationLabel];
        _userNameLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:1];
        _userNameLabel.textAlignment = NSTextAlignmentCenter;
        _userNameLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_userNameLabel];
        _statusLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:1];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        _statusLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_statusLabel];
        
        
        typeof(self) __weak weakSelf = self;
        [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left);
        }];
        [_durationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_amountLabel.mas_right).with.offset(5.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.width.equalTo(_amountLabel.mas_width);
        }];
        [_valuationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_durationLabel.mas_right).with.offset(5.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.width.equalTo(_durationLabel.mas_width);
        }];
        [_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_valuationLabel.mas_right).with.offset(5.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.width.equalTo(_valuationLabel.mas_width);
        }];
        [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_userNameLabel.mas_right).with.offset(5.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.right.equalTo(weakSelf.contentView.mas_right);
            make.width.equalTo(_userNameLabel.mas_width);
        }];

    }
    return self;

}
- (void)prepareForReuse {
    [super prepareForReuse];
    self.amountLabel.text = nil;
    self.durationLabel.text = nil;
    self.valuationLabel.text = nil;
    self.userNameLabel.text = nil;
    self.statusLabel.text = nil;

}
-(void)configureWithLoan:(PHLoan *)loan{
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.durationLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)loan.duration.unsignedIntegerValue];
    self.amountLabel.text = [loan phc_amountString];
    self.valuationLabel.text = [loan phc_valuationString];
    self.userNameLabel.text = loan.applicantName;
    self.statusLabel.text = NSLocalizedString(loan.status, nil);



}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
