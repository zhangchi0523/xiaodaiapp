//
//  PHSearchViewController.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/13.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewController.h"
extern NSString * const PHNotificationUpdatecommunity;
extern NSString * const PHNotificationUpdatehouse;
typedef enum :NSUInteger{
    SearchCommunity =1,
    SearchHouse = 2,
    
}SearchStatus;
@interface PHSearchViewController : PHTableViewController
@property (nonatomic, strong) NSString *cityString;
@property (nonatomic, strong) NSArray *houseNumberListArray;
@property (nonatomic, strong) NSString *searchType;

@end
