//
//  UIColor+phc_addition.h
//  p2p
//
//  Created by Philip on 10/16/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface UIColor (phc_addition)

+ (UIColor *)phc_blue;

+ (UIColor *)phc_gray;

+ (UIColor *)phc_lightGray;

+ (UIColor *)phc_green;

+ (UIColor *)phc_lightBlue;
+ (UIColor *)phc_newGray;
@end
