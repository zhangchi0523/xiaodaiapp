//
//  UIFont+phc_addition.h
//  p2p
//
//  Created by Philip on 9/6/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (phc_addition)

+ (UIFont *)phc_defaultFontWithSize:(CGFloat)size;

+ (UIFont *)phc_defaultBoldFontWithSize:(CGFloat)size;

@end
