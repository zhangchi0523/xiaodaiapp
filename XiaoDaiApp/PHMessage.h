//
//  PHMessage.h
//  p2p
//
//  Created by Philip on 9/15/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

typedef NS_ENUM(NSUInteger, PHMessageType) {
    PHSystemMessage = 1,
    PHCreditMessage,
    PHLoanMessage
};

@interface PHMessage : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, readwrite) NSString *message;
@property (nonatomic, copy, readwrite) NSDate *date;
@property (nonatomic, copy, readwrite) NSNumber *ID;
@property (nonatomic, copy, readwrite) NSNumber *borrowId;
@property (nonatomic, assign, readwrite) BOOL isread;
@property (nonatomic, copy, readwrite) NSString* detailFlag;
@property (nonatomic, assign, readwrite) enum PHMessageType type;

@end
