//
//  PHPickerViewController.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHPickerViewController : UIViewController

@property (nonatomic, strong)NSString *dataString;

- (instancetype)initWithContentArray:(NSArray *)array DismissHandler:(void(^)(PHPickerViewController *controller))dismissHandler;

@end
