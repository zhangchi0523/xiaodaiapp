
//
//  PHObjectManager.m
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import <AFNetworking/AFNetworking.h>
#import <BlocksKit/BlocksKit.h>
#import <Mantle/Mantle.h>
#import "PHAPIBaseResponse.h"
#import "PHAPILoginResponse.h"
#import "PHSessionManager.h"
#import "PHAPIProvinceListResponse.h"
#import "PHAPIValuationResponse.h"
#import "PHApplications.h"
#import "PHAPIGetLoanListResponse.h"
#import "PHAPILoanDetailResponse.h"
#import "PHAPIGetBranchResponse.h"
NSString *const ErrorDomain = @"com.hfax";
NSString *const HTTPErrorDomain = @"com.alamofire.error.serialization.response";
@interface PHObjectManager()

@property (nonatomic, strong) AFHTTPSessionManager *httpManager;
@property (nonatomic, strong) BFTaskCompletionSource *bankCardCompletionSource;
@property (nonatomic, strong) BFTaskCompletionSource *creditMaskCompletionSource;
@property (nonatomic, strong) BFTaskCompletionSource *syncContactsCompletionSource;

@end

@implementation PHObjectManager

+ (instancetype)sharedInstance {
    static PHObjectManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [PHObjectManager new];
    });

    return instance;

}

- (void)ignoreSSL {
    self.httpManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    self.httpManager.securityPolicy.allowInvalidCertificates = YES;
    self.httpManager.securityPolicy.validatesDomainName = NO;
}

- (instancetype)init {
    if (self = [super init]) {
        self.httpManager = [[AFHTTPSessionManager manager] initWithBaseURL:self.baseURL];
        [self ignoreSSL];
        
        self.httpManager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
        self.httpManager.responseSerializer.acceptableContentTypes =  [NSSet setWithObjects:@"application/json", @"text/html",@"text/json",@"text/javascript", nil];
        [self.httpManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.httpManager.requestSerializer setValue:@"postman" forHTTPHeaderField:@"X-Requested-With"];
}

    return self;
}

- (NSURL *)baseURL {
    return [NSURL URLWithString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHBaseUrl"]];
}

- (void)changeBaseURL:(NSURL *)newBaseURL {
    if (self.httpManager) {
        [self.httpManager invalidateSessionCancelingTasks:YES];
        [self.httpManager.operationQueue cancelAllOperations];
    }
    
    self.httpManager = [[AFHTTPSessionManager alloc] initWithBaseURL:newBaseURL];
    [self ignoreSSL];
}

- (BFTask *)taskToLoginUseName:(NSString *)useName passWord:(NSString *)passWord {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"username":useName,@"password":passWord};
    
    [self performRequestWithMethod:@"GET" urlString:@"/api/login" mappingClass:PHAPILoginResponse.class parameters:parm completionSource:completionSource];
    
    return completionSource.task;
}

- (BFTask *)taskToProvincesList {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    [self taskURLString:@"/api/valuation/provinces" parameters:nil method:@"GET" completionSource:completionSource];
    
    return completionSource.task;
}

- (BFTask *)taskToCityList:(NSString *)provomces {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"city":provomces};
    [self taskURLString:@"/api/valuation/cities" parameters:parm method:@"GET" completionSource:completionSource];
    
    return completionSource.task;
}

- (BFTask *)taskToCommunityList:(NSString *)city communityName:(NSString *)name{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"city":city,@"name":name};
    [self taskURLString:@"/api/valuation/constructions" parameters:parm method:@"GET" completionSource:completionSource];
    return completionSource.task;
}

- (BFTask *)taskToHouseList:(NSString *)city construction:(NSString *)construction{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"city":city,@"construction":construction};
    [self taskURLString:@"/api/valuation/buildings" parameters:parm method:@"GET" completionSource:completionSource];
    return completionSource.task;
}

- (BFTask *)taskToRoomList:(NSString *)city building:(NSString *)building{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"city":city,@"building":building};
    [self taskURLString:@"/api/valuation/houses" parameters:parm method:@"GET" completionSource:completionSource];
    return completionSource.task;
}

- (BFTask *)taskToPriceCity:(NSString *)city construction:(NSString *)construction building:(NSString *)building room:(NSString *)room size:(NSString *)size{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary *parm = @{@"city":city,@"construction":construction,@"building":building,@"room":room,@"size":size};
    
//    [self taskURLString:@"/api/valuation/price" parameters:parm method:@"GET" completionSource:completionSource];
    
    [self performRequestWithMethod:@"GET" urlString:@"/api/valuation/price" mappingClass:PHAPIValuationResponse.class parameters:parm completionSource:completionSource];

    return completionSource.task;
}
-(BFTask *)taskToGetLoanList:(NSString *)page{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];

    NSDictionary *parm = @{@"page":page};
    [self performRequestWithMethod:@"GET" urlString:@"/api/applications/page" mappingClass:PHAPIGetLoanListResponse.class parameters:parm completionSource:completionSource];
    return completionSource.task;
}
-(BFTask *)taskToGetLoanDetail:(NSString *)appId{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    NSDictionary *parm = @{@"appId":appId};
    [self performRequestWithMethod:@"GET" urlString:@"/api/applications/appId" mappingClass:PHAPILoanDetailResponse.class parameters:parm completionSource:completionSource];
    return completionSource.task;
}
-(BFTask *)taskToGetBranch{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    [self performRequestWithMethodforBranch:@"GET" urlString:@"/api/branches" mappingClass:PHAPIGetBranchResponse.class parameters:nil completionSource:completionSource];
    return completionSource.task;

}
- (BFTask *)taskToRegisterAccountWithName:(NSString *)name PhoneNumber:(NSString *)phoneNumber password:(NSString *)password identificationNumber:(NSString *)identificationNumber withBranchId:(NSString *)branchId {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    NSDictionary* param = @{@"name":name,@"phone":phoneNumber,@"password":password,@"identificationNumber":identificationNumber,@"branch":@{@"id":branchId}};
    [self performRequestWithMethod:@"PUT" urlString:@"/api/users" mappingClass:PHAPILoginResponse.class parameters:param completionSource:completionSource];
    
    return completionSource.task;
}
-(BFTask *)taskToApplications:(PHApplications *)application{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];

        NSDictionary *param = @{@"name":application.name,@"amount":application.amount,@"phone":application.phone,@"identificationNumber":application.identificationNumber,@"identificationFront":application.identificationFront,@"identificationBack":application.identificationBack,@"hukouFront":application.hukouFront,@"hukouBack":application.hukouBack,@"deed":application.deed,@"type":application.type,@"valuationId":application.valuationId,@"address":application.address,@"otherMedia":application.imageArray};
    [self performRequestWithMethod:@"PUT" urlString:@"/api/applications" mappingClass:PHAPIBaseResponse.class parameters:param completionSource:completionSource];
    return completionSource.task;
}
-(BFTask *)taskToChangePassword:(NSString *)password{
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    [self.httpManager.requestSerializer setValue:[PHSessionManager sharedInstance].user_token forHTTPHeaderField:@"X-AUTH-TOKEN"];
    NSDictionary *param = @{@"password":password};
    [self performRequestWithMethod:@"PUT" urlString:@"/api/password" mappingClass:PHAPIBaseResponse.class parameters:param completionSource:completionSource];
    return completionSource.task;
}
#pragma mark -
#pragma mark - Utils

- (void)setCompletionSourceAfterDelay:(BFTaskCompletionSource *)completionSource {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [completionSource setResult:@YES];
    });
}
- (void)mapResponse:(NSDictionary*)data toClass:(Class)class withSuccessHandler:(void(^)(PHAPIBaseResponse* response))successHandler errorHandler:(void (^)(NSError* error))errorHandler {
    if (![class isSubclassOfClass:PHAPIBaseResponse.class]) {
        [[NSException exceptionWithName:NSInvalidArgumentException reason:@"Response class is not a subclass of PHAPIBaseResponse" userInfo:nil] raise];
    }
    
    NSError* error = nil;
    PHAPIBaseResponse *mappedResponse = [MTLJSONAdapter modelOfClass:class fromJSONDictionary:data error:&error];
    
    if (error) {
        if (errorHandler) {
            errorHandler(error);
        }
    } else if (mappedResponse.errorCode) {
        if (errorHandler) {
            NSString *errorMessage = mappedResponse.message.length ? mappedResponse.message : @"出现未知错误";
            errorHandler([NSError errorWithDomain:ErrorDomain code:mappedResponse.errorCode userInfo:@{NSLocalizedDescriptionKey: errorMessage}]);
        }
    } else if (mappedResponse) {
        if (successHandler) {
            successHandler(mappedResponse);
        }
    } else {
        if (successHandler) {
            successHandler(mappedResponse);
        }
    }
}
- (BFTask *)taskURLString:(NSString *)urlString parameters:parameters method:(NSString *)method completionSource:(BFTaskCompletionSource *)completionSource{
    NSURLSessionDataTask *task = nil;

    if ([method isEqualToString:@"GET"]) {
        task = [self.httpManager GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [completionSource setResult:responseObject];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [completionSource setError:error];
        }];
    } else if ([method isEqualToString:@"POST"]) {
        task = [self.httpManager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:nil failure:nil];
    } else if ([method isEqualToString:@"PUT"]) {
        task = [self.httpManager PUT:urlString parameters:parameters success:nil failure:nil];
    } else if ([method isEqualToString:@"DELETE"]) {
        task = [self.httpManager DELETE:[self urlString:urlString withParams:parameters] parameters:parameters success:nil failure:nil];
    }

    return completionSource.task;
}
- (NSURLSessionDataTask *)performRequestWithMethod:(NSString *)method urlString:(NSString *)URLString mappingClass:(Class)mappingClass parameters:(NSDictionary *)parameters completionSource:(BFTaskCompletionSource *)completionSource {
    NSURLSessionDataTask *task = nil;
    
    typeof(self) __weak weakSelf = self;
    
    void (^succcessHandler)(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) = ^void(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        [weakSelf mapResponse:responseObject toClass:mappingClass withSuccessHandler:^(PHAPIBaseResponse *response) {
            [completionSource trySetResult:response];
        } errorHandler:^(NSError *error) {
            [completionSource trySetError:error];
        }];
    };
    
    void (^errorHandler)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) = ^void(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        [completionSource trySetError:error];
    };
    
    if ([method isEqualToString:@"GET"]) {
        task = [self.httpManager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress){
        } success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"POST"]) {
        task = [self.httpManager POST:URLString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"PUT"]) {
        task = [self.httpManager PUT:URLString parameters:parameters success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"DELETE"]) {
        task = [self.httpManager DELETE:[self urlString:URLString withParams:parameters] parameters:parameters success:succcessHandler failure:errorHandler];
    }
    
    return task;
}

- (NSString*)urlString:(NSString*) url withParams:(NSDictionary*) params {
    NSMutableString* string = [NSMutableString stringWithString:url];
    [string appendString:@"?"];
    for (NSString* key in params) {
        NSString* value = [NSString stringWithFormat:@"%@", [params objectForKey:key]];
        NSString *encodedValue = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                    (CFStringRef)value, nil,
                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
        if (key && ![value isEqual:NSNull.null]) {
            [string appendString:[NSString stringWithFormat:@"%@=%@&", key, encodedValue]];
        } else {
            [string appendString:[NSString stringWithFormat:@"%@&", key]];

        }
    }

    return [string substringToIndex:string.length - 1];    
}
#pragma mark -for Branches API
- (void)mapResponseForBranch:(NSDictionary*)data toClass:(Class)class withSuccessHandler:(void(^)(PHAPIGetBranchResponse* response))successHandler errorHandler:(void (^)(NSError* error))errorHandler {
    if (![class isSubclassOfClass:PHAPIGetBranchResponse.class]) {
        [[NSException exceptionWithName:NSInvalidArgumentException reason:@"Response class is not a subclass of PHAPIGetBranchResponse" userInfo:nil] raise];
    }
    
    NSError* error = nil;
    PHAPIGetBranchResponse *mappedResponse = [MTLJSONAdapter modelOfClass:class fromJSONDictionary:data error:&error];
    
    if (error) {
        if (errorHandler) {
            errorHandler(error);
        }
    } else if (mappedResponse.errorCode) {
        if (errorHandler) {
            NSString *errorMessage = mappedResponse.message.length ? mappedResponse.message : @"出现未知错误";
            errorHandler([NSError errorWithDomain:ErrorDomain code:mappedResponse.errorCode userInfo:@{NSLocalizedDescriptionKey: errorMessage}]);
        }
    } else if (mappedResponse) {
        if (successHandler) {
            successHandler(mappedResponse);
        }
    } else {
        if (successHandler) {
            successHandler(mappedResponse);
        }
    }
}

- (NSURLSessionDataTask *)performRequestWithMethodforBranch:(NSString *)method urlString:(NSString *)URLString mappingClass:(Class)mappingClass parameters:(NSDictionary *)parameters completionSource:(BFTaskCompletionSource *)completionSource {
    NSURLSessionDataTask *task = nil;
    
    typeof(self) __weak weakSelf = self;
    
    void (^succcessHandler)(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) = ^void(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        [weakSelf mapResponseForBranch:responseObject toClass:mappingClass withSuccessHandler:^(PHAPIGetBranchResponse *response) {
            [completionSource trySetResult:response];
        } errorHandler:^(NSError *error) {
            [completionSource trySetError:error];
        }];
    };
    
    void (^errorHandler)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) = ^void(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        [completionSource trySetError:error];
    };
    
    if ([method isEqualToString:@"GET"]) {
        task = [self.httpManager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress){
        } success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"POST"]) {
        task = [self.httpManager POST:URLString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"PUT"]) {
        task = [self.httpManager PUT:URLString parameters:parameters success:succcessHandler failure:errorHandler];
    } else if ([method isEqualToString:@"DELETE"]) {
        task = [self.httpManager DELETE:[self urlString:URLString withParams:parameters] parameters:parameters success:succcessHandler failure:errorHandler];
    }
    
    return task;
}




@end
