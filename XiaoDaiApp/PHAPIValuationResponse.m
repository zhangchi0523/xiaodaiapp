//
//  PHAPIValuationResponse.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIValuationResponse.h"
#import "PHValuationModel.h"

@implementation PHAPIValuationResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    [mapping setObject:@"data" forKey:@"AvagePrice"];
    return mapping;
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:PHValuationModel.class];
}

@end
