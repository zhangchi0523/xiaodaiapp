//
//  PHParsingJSONDictionary.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PHParsingJSONDictionary : MTLModel

@property (nonatomic, readonly, copy) NSString *ProvinceId;
@property (nonatomic, readonly, copy) NSString *ProvinceName;

@end
