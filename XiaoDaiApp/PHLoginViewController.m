//
//  PHLoginViewController.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLoginViewController.h"
#import "UIColor+phc_addition.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHTextFieldTableViewCell.h"
#import "PHKeyValueTableViewCell.h"
#import "UIControl+BlocksKit.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import "UILabel+phc_addition.h"
#import "UIView+BlocksKit.h"
#import "PHRegisterViewController.h"
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHUserProfile.h"
#import "PHAPILoginResponse.h"
#import "PHSessionManager.h"
#import "UITextField+phc_addition.h"
#import "UIFont+phc_addition.h"

static NSString * const kAuthUserKey = @"auth_user";
static NSString * const kAuthToken = @"auth_token";


@interface PHLoginViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UILabel* freeRegistrationLabel;
@property (nonatomic, strong) UILabel* forgotPasswordLabel;
@property (nonatomic, strong) PHObjectManager *objectManager;
@property (nonatomic, strong) PHSessionManager *sessionManager;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong, readwrite) PHUserProfile* currentUserProfile;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@end

@implementation PHLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImageView *backgroundImageView = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    backgroundImageView.image = [UIImage imageNamed:@"backgroundImage"];
    [self.view addSubview:backgroundImageView];
    
    typeof(self) __weak weakSelf = self;
    self.objectManager = [PHObjectManager sharedInstance];
    self.sessionManager = [PHSessionManager sharedInstance];
    
    self.phoneTextField = [UITextField phc_defaultFieldWithPlaceholderText:@"请输入手机号码" text:nil];
    self.phoneTextField.delegate = self;
    [self.view addSubview:self.phoneTextField];
    
    self.passwordTextField = [UITextField phc_defaultFieldWithPlaceholderText:@"请输入登录密码" text:nil];
    self.passwordTextField.delegate = self;
    self.passwordTextField.secureTextEntry = YES;
    [self.view addSubview:self.passwordTextField];
    
    UIView *lineView = [UIView new];
    UIView *lineView2 = [UIView new];
    lineView.backgroundColor = [UIColor phc_lightBlue];
    lineView2.backgroundColor = [UIColor phc_lightBlue];
    [self.view addSubview:lineView];
    [self.view addSubview:lineView2];
    
    self.loginButton = [UIButton phc_actionButtonWithTitle:@"登录"];
    [self.loginButton addTarget:self action:@selector(loginButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.loginButton];
    
    self.forgotPasswordLabel = [UILabel phc_labelWithFontSize:14 textColor:[UIColor phc_lightBlue] backgroundColor:nil numberOfLines:0];
    self.forgotPasswordLabel.text = @"忘记密码?";
    self.forgotPasswordLabel.userInteractionEnabled = YES;
    self.forgotPasswordLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:self.forgotPasswordLabel];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"还没账号? 免费注册"];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor phc_lightBlue] range:NSMakeRange(0,5)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(5,5)];
    
    self.freeRegistrationLabel = [UILabel phc_labelWithFontSize:14 textColor:[UIColor phc_lightBlue] backgroundColor:nil numberOfLines:0];
    self.freeRegistrationLabel.attributedText = str;
    self.freeRegistrationLabel.userInteractionEnabled = YES;
    self.freeRegistrationLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.freeRegistrationLabel];
    
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@209);
        make.left.equalTo(@30);
        make.height.equalTo(@44);
        make.right.equalTo(@(-30));
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.phoneTextField.mas_top).with.offset(44);
        make.left.equalTo(weakSelf.phoneTextField.mas_left);
        make.right.equalTo(weakSelf.phoneTextField.mas_right);
        make.height.equalTo(@0.5);
    }];
    
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.phoneTextField.mas_top).with.offset(44);
        make.left.equalTo(weakSelf.phoneTextField.mas_left);
        make.right.equalTo(weakSelf.phoneTextField.mas_right);
        make.height.equalTo(weakSelf.phoneTextField.mas_height);
    }];
    
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.passwordTextField.mas_top).with.offset(44);
        make.left.equalTo(weakSelf.passwordTextField.mas_left);
        make.right.equalTo(weakSelf.passwordTextField.mas_right);
        make.height.equalTo(@0.5);
    }];
    
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView2.mas_top).with.offset(60);
        make.left.equalTo(lineView2.mas_left);
        make.right.equalTo(lineView2.mas_right);
        make.height.equalTo(@44);
    }];
    
    [self.forgotPasswordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView2.mas_top).with.offset(15);
        make.right.equalTo(lineView2.mas_right);
        make.width.equalTo(@80);
        make.height.equalTo(@14);
    }];
    
    [self.freeRegistrationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.view.mas_bottom).with.offset(-30);
        make.width.equalTo(weakSelf.view.mas_width);
        make.height.equalTo(@14);
    }];
    
    [self.freeRegistrationLabel bk_whenTapped:^{
        PHRegisterViewController *vc = [PHRegisterViewController new];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.forgotPasswordLabel bk_whenTapped:^{
    }];
}

- (void)loginButtonTapped:(UIButton *)button {
    typeof(self) __weak weakSelf = self;
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[self.objectManager taskToLoginUseName:self.phoneTextField.text passWord:self.passwordTextField.text] continueWithBlock:^id(BFTask *task) {
            [weakSelf.hud hideAnimated:YES];
            if (task.error) {
                [self taskToShowError:task.error];
            }else {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                weakSelf.currentUserProfile = ((PHAPILoginResponse*)task.result).user;
                [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:weakSelf.currentUserProfile] forKey:kAuthUserKey];
                [userDefaults setObject:[[task.result valueForKey:@"data"] valueForKey:@"token"] forKey:kAuthToken];
                self.sessionManager.user_token = [[task.result valueForKey:@"data"] valueForKey:@"token"];
                if (self.successHandler) {
                    self.successHandler(self);
                }
            }
            return task;
        }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
