//
//  PHAPILoanDetailResponse.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/5.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPILoanDetailResponse.h"
@interface PHAPILoanDetailResponse()
@property (nonatomic, readwrite, copy) NSNumber *amount;
@property (nonatomic, readwrite, copy) NSNumber *duration;
@property (nonatomic, readwrite, copy) NSNumber *valuation;
@property (nonatomic, readwrite, copy) NSString *applicantName;
@property (nonatomic, readwrite, copy) NSString *status;

@property (nonatomic, readwrite, copy) NSString *applicantPhone;
@property (nonatomic, readwrite, copy) NSString *applicantIdentificationNumber;
@property (nonatomic, readwrite, copy) NSString *agentName;
@property (nonatomic, readwrite, copy) NSString *agentPhone;
@property (nonatomic, readwrite, copy) NSString *agentIdentificationNumber;
@property (nonatomic, readwrite, copy) NSString *address;
@property (nonatomic, readwrite, copy) NSString *type;

@property (nonatomic, readwrite, copy) NSNumber *buildArea;
@property (nonatomic, readwrite, copy) NSNumber *unitPrice;
@property (nonatomic, readwrite, copy) NSNumber *avagePrice;
@property (nonatomic, readwrite, copy) NSNumber *maxPrice;
@property (nonatomic, readwrite, copy) NSNumber *minPrice;
@property (nonatomic, readwrite, copy) NSNumber *appId;
@property (nonatomic, readwrite, copy) NSString *identificationFront;
@property (nonatomic, readwrite, copy) NSString *identificationBack;
@property (nonatomic, readwrite, copy) NSString *huKouFront;
@property (nonatomic, readwrite, copy) NSString *hukouBack;
@property (nonatomic, readwrite, copy) NSString *deed;
@property (nonatomic, strong, readwrite) NSArray *mediaArray;

@property (nonatomic, readwrite, copy) NSNumber *branchId;
@property (nonatomic, readwrite, copy) NSString *branchName;

@end
@implementation PHAPILoanDetailResponse
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey]mutableCopy];
    [mapping setObject:@"data.id" forKey:@"appId"];
    [mapping setObject:@"data.amount" forKey:@"amount"];
    [mapping setObject:@"data.duration" forKey:@"duration"];
    [mapping setObject:@"data.valuation.Amount" forKey:@"valuation"];
    [mapping setObject:@"data.applicant.name" forKey:@"applicantName"];
    [mapping setObject:@"data.status" forKey:@"status"];
    [mapping setObject:@"data.applicant.phone" forKey:@"applicantPhone"];
    [mapping setObject:@"data.applicant.identificationNumber" forKey:@"applicantIdentificationNumber"];
    [mapping setObject:@"data.agent.name" forKey:@"agentName"];
    [mapping setObject:@"data.agent.phone" forKey:@"agentPhone"];
    [mapping setObject:@"data.agent.agentIdentificationNumber" forKey:@"agentIdentificationNumber"];
    [mapping setObject:@"data.agent.branch.id" forKey:@"branchId"];
    [mapping setObject:@"data.agent.branch.name" forKey:@"branchName"];

    
    [mapping setObject:@"data.type" forKey:@"type"];
    [mapping setObject:@"data.address" forKey:@"address"];
    [mapping setObject:@"data.valuation.BuildArea" forKey:@"buildArea"];
    [mapping setObject:@"data.valuation.UnitPrice" forKey:@"unitPrice"];
    [mapping setObject:@"data.valuation.AvagePrice" forKey:@"avagePrice"];
    [mapping setObject:@"data.valuation.MaxPrice" forKey:@"maxPrice"];
    [mapping setObject:@"data.valuation.MinPrice" forKey:@"minPrice"];
    [mapping setObject:@"data.applicationMedia.identificationFront.source" forKey:@"identificationFront"];
    [mapping setObject:@"data.applicationMedia.identificationBack.source" forKey:@"identificationBack"];
    [mapping setObject:@"data.applicationMedia.huKouFront.source" forKey:@"huKouFront"];
    [mapping setObject:@"data.applicationMedia.hukouBack.source" forKey:@"hukouBack"];
    [mapping setObject:@"data.applicationMedia.deed.source" forKey:@"deed"];
    [mapping setObject:@"data.applicationMedia.extraMedia" forKey:@"mediaArray"];

    
    return mapping;
}
- (NSNumberFormatter *)currencyFormatter {
    static NSNumberFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.groupingSeparator = @",";
        formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
    });
    
    return formatter;
}
- (NSString *)phc_amountString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.amount doubleValue]]]];
}
- (NSString *)phc_valuationString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.valuation doubleValue]]]];
}
- (NSString *)phc_unitPriceString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.unitPrice doubleValue]]]];

}
- (NSString *)phc_minPriceString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.minPrice doubleValue]]]];

}
- (NSString *)phc_maxPriceString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.maxPrice doubleValue]]]];

}
- (NSString *)phc_avagePriceString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.avagePrice doubleValue]]]];

}
-(NSString *)stringByTrimming:(NSString *)str{
    NSString *subStr = [str substringWithRange:NSMakeRange(0, 1)];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:subStr];
    return [str stringByTrimmingCharactersInSet:set];
}
@end
