//
//  PHLOanListDetailViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLOanListDetailViewController.h"
#import "UIColor+phc_addition.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHKeyValueTableViewCell.h"
#import "UILabel+phc_addition.h"
#import "PHLoan.h"
#import "PHTitleTableViewCell.h"
#import "PHAPILoanDetailResponse.h"
#import <Masonry.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import "UIButton+phc_addition.h"
#import "UIImage+phc_addition.h"
#import "PHLagerViewTool.h"
@interface PHLOanListDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) PHAPILoanDetailResponse *loan;
@property(nonatomic,strong)UICollectionView *collectionListViewOne;
@property(nonatomic,strong)UICollectionView *collectionListViewTwo;

@end

@implementation PHLOanListDetailViewController

- (instancetype)initWithLoan:(id)loan {
    if (self = [super init]) {
        self.navigationItem.title = @"申请详情";
        _loan = loan;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    typeof(self) __weak weakSelf = self;

    self.view.backgroundColor = [UIColor phc_lightGray];
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTitleTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTitleTableViewCell *c = (PHTitleTableViewCell *)cell;
                           [c configureWithTitle:@"申请人信息" textColor:nil];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"姓名" details:weakSelf.loan.applicantName textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"电话" details:weakSelf.loan.applicantPhone textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"身份证" details:weakSelf.loan.applicantIdentificationNumber textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTitleTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTitleTableViewCell *c = (PHTitleTableViewCell *)cell;
                           [c configureWithTitle:@"业务人员信息" textColor:nil];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"姓名" details:weakSelf.loan.agentName textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"电话" details:weakSelf.loan.agentPhone textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"渠道号" details:[weakSelf.loan.branchName stringByRemovingPercentEncoding] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTitleTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTitleTableViewCell *c = (PHTitleTableViewCell *)cell;
                           [c configureWithTitle:@"贷款申请信息" textColor:nil];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"金额(元)" details:[weakSelf.loan phc_amountString] textColor:[UIColor blackColor]];
                           
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"地址" details:weakSelf.loan.address textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"期限" details:[NSString stringWithFormat:@"%d",weakSelf.loan.duration.intValue] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"种类" details:NSLocalizedString(weakSelf.loan.type, nil) textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"状态" details:NSLocalizedString(weakSelf.loan.status, nil) textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTitleTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTitleTableViewCell *c = (PHTitleTableViewCell *)cell;
                           [c configureWithTitle:@"抵押估值" textColor:nil];
                           
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"金额(元)" details:[weakSelf.loan phc_amountString] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"面积(平米)" details:[NSString stringWithFormat:@"%d",weakSelf.loan.buildArea.integerValue] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"均价(元)" details:[weakSelf.loan phc_unitPriceString] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"最低价(元)" details:[weakSelf.loan phc_minPriceString] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"最高价(元)" details:[weakSelf.loan phc_maxPriceString] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],[[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"单价(元)" details:[weakSelf.loan phc_avagePriceString] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       ];
    
    [self.dataSource addItems:items];
    [self setUpPictureView];
    // Do any additional setup after loading the view.
}
-(void)setUpPictureView{
    typeof(self) __weak weakSelf = self;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 440.0f)];
    UILabel *informationLabel = [UILabel phc_labelWithBoldFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
    informationLabel.textAlignment = NSTextAlignmentLeft;
    informationLabel.text = @"证件资料";
    [footerView addSubview:informationLabel];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    _collectionListViewOne= [[UICollectionView alloc]initWithFrame:CGRectMake(0,0,0,0) collectionViewLayout:layout];
    _collectionListViewOne.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:self.collectionListViewOne];
    [self.collectionListViewOne registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionListViewOne.delegate = self;
    self.collectionListViewOne.dataSource = self;
    
    UILabel *informationLabelTwo = [UILabel phc_labelWithBoldFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
    informationLabelTwo.textAlignment = NSTextAlignmentLeft;
    informationLabelTwo.text = @"其他资料";
    [footerView addSubview:informationLabelTwo];
    
    UICollectionViewFlowLayout *layoutTwo = [[UICollectionViewFlowLayout alloc] init];
    [layoutTwo setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _collectionListViewTwo= [[UICollectionView alloc]initWithFrame:CGRectMake(0,0,0,0) collectionViewLayout:layoutTwo];
    _collectionListViewTwo.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:self.collectionListViewTwo];
    [self.collectionListViewTwo registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionListViewTwo.delegate = self;
    self.collectionListViewTwo.dataSource = self;
    
    self.tableView.tableFooterView= footerView;
    if(self.loan.mediaArray.count ==0){
        informationLabelTwo.hidden = YES;
        self.collectionListViewTwo.hidden = YES;
    }
    
    [informationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(footerView.mas_top).with.offset(10.0f);
        make.left.equalTo(footerView.mas_left).with.offset(10.0f);
        make.width.equalTo(weakSelf.view.mas_width).with.multipliedBy(0.5f);
        make.height.equalTo(@(20.0f));
    }];
    [self.collectionListViewOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(informationLabel.mas_bottom).with.offset(10.0f);
        make.left.equalTo(footerView.mas_left).with.offset(10.0f);
        make.right.equalTo(footerView.mas_right).with.offset(-10.0f);
        make.height.equalTo(@240);
    }];
    [informationLabelTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionListViewOne.mas_bottom).with.offset(10.0f);
        make.left.equalTo(footerView.mas_left).with.offset(10.0f);
        make.width.equalTo(weakSelf.view.mas_width).with.multipliedBy(0.5f);
        make.height.equalTo(@(20.0f));
    }];
    [self.collectionListViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(informationLabelTwo.mas_bottom).with.offset(10.0f);
        make.left.equalTo(footerView.mas_left).with.offset(10.0f);
        make.right.equalTo(footerView.mas_right).with.offset(-10.0f);
        make.height.equalTo(@120);
    }];
}

#pragma mark collectionView代理方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView == self.collectionListViewOne){
        return 2;
    }else{
        return (self.loan.mediaArray.count/3+1);
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionListViewTwo&&section==(self.loan.mediaArray.count/3)) {
        return (self.loan.mediaArray.count%3);
    }else{
        return 3;
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *imageView = nil;
    if (collectionView == self.collectionListViewOne) {
        switch (indexPath.section) {
            case 0:
                switch (indexPath.row) {
                    case 0:
                        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.identificationFront]]]];
                        break;
                    case 1:
                        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.identificationBack]]]];
                        break;
                    case 2:
                        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.huKouFront]]]];
                        break;
                    default:
                        break;
                }
                break;
            case 1:
                switch (indexPath.row) {
                    case 0:
                        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.hukouBack]]]];
                        break;
                    case 1:
                        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.deed]]]];
                        break;
                    case 2:
                        cell.contentView.hidden = YES;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }else{
        imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[self.loan.mediaArray objectAtIndex:indexPath.section*3+indexPath.row] objectForKey:@"source"]]]]];
    }
    
    [cell.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(cell.contentView.mas_top).with.offset(10.0f);
        make.left.equalTo(cell.contentView.mas_left);
        make.right.equalTo(cell.contentView.mas_right);
        make.bottom.equalTo(cell.contentView.mas_bottom).with.offset(-10.0f);
    }];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width*0.25,self.view.frame.size.width*0.3);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionListViewOne) {
        switch (indexPath.section) {
            case 0:{
                switch (indexPath.row) {
                    case 0:{
                        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.identificationFront]]]];
                        [tool showLagerView];
                        break;
                    }
                    case 1:{
                        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.identificationBack]]]];
                        [tool showLagerView];

                        break;
                    }
                    case 2:{
                        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.huKouFront]]]];
                        [tool showLagerView];

                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case 1:{
                switch (indexPath.row) {
                    case 0:{
                        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.hukouBack]]]];
                        [tool showLagerView];

                        break;
                    }
                    case 1:{
                        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.loan.deed]]]];
                        [tool showLagerView];

                        break;
                    }
                    default:
                        break;
                }
                break;
            }
                
                
            default:
                break;
        }
    }
    else{
        PHLagerViewTool *tool= [[PHLagerViewTool alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[self.loan.mediaArray objectAtIndex:indexPath.section*3+indexPath.row] objectForKey:@"source"]]]]];
        [tool showLagerView];

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
