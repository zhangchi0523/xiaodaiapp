//
//  PHTextFieldTableViewCell.m
//  p2p
//
//  Created by Philip on 10/21/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTextFieldTableViewCell.h"
#import "UITextField+phc_addition.h"
#import "UILabel+phc_addition.h"
#import <Masonry/Masonry.h>

@interface PHTextFieldTableViewCell()<UITextFieldDelegate>
@property (nonatomic, strong, readwrite) UITextField *textField;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,assign) BOOL idNumTextField;
@end

@implementation PHTextFieldTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [UILabel phc_labelWithFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
        _titleLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_titleLabel];
        
        _textField = [UITextField phc_textFieldWithFontSize:15.0f textColor:[UIColor darkGrayColor] placeholderText:nil text:nil];
        _textField.delegate = self;
        [self.contentView addSubview:_textField];
        
        typeof(self) __weak weakSelf = self;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.contentView.mas_width).with.multipliedBy(0.3f);
        }];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.right.equalTo(weakSelf.contentView.mas_right).with.offset(-10.0f);
            make.left.equalTo(_titleLabel.mas_right).with.offset(25.0f);
        }];
    }
    
    return self;
}

- (void)configureWithTitle:(NSString *)title placeholder:(NSString *)placeholder text:(NSString *)text {
    self.titleLabel.text = title;
    self.textField.placeholder = placeholder;
    self.textField.text = nil;
}
- (void)configureWithKeyBoardType:(UIKeyboardType)keyBoardType withIdNumTextField:(BOOL)IdNumTextField{
    if (keyBoardType) {
        self.textField.keyboardType = keyBoardType;
    }
    self.idNumTextField = IdNumTextField;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.textField.text = nil;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (self.idNumTextField == YES) {
        NSString *regex = @"(^[X-Xx-x0-9]{0,17}$)";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        if (![pred evaluateWithObject:string]) {
            return NO;
        }
    }
    return YES;
}
@end
