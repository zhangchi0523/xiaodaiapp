//
//  PHLoan+String.h
//  p2p
//
//  Created by Philip on 9/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHLoan.h"

@interface PHLoan (phc_string)

- (NSString *)phc_amountString;
- (NSString *)phc_valuationString;


@end
