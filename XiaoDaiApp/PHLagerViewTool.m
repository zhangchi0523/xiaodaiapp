//
//  PHLagerViewTool.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/10.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLagerViewTool.h"
#import "UIImage+phc_addition.h"
#import "UIButton+phc_addition.h"
#import <BlocksKit/BlocksKit+UIKit.h>
@interface PHLagerViewTool()
@property(nonatomic,strong)UIImage *containImage;
@end
@implementation PHLagerViewTool

- (instancetype)initWithImage:(UIImage *)image
{
    self = [super init];
    if (self) {
        _containImage = [image copy];
    }
    return self;
}
-(void)showLagerView{
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    NSInteger screenWidth = screenFrame.size.width;
    NSInteger screenHeight = screenFrame.size.height;
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    bgView.alpha = 0.95f;
    bgView.backgroundColor = [UIColor blackColor];
    UIButton *closeButton = [UIButton phc_buttonWithFontSize:16.0f title:@"关闭" textColor:[UIColor whiteColor] backgroundImage:[UIImage phc_imageWithColor:[UIColor blackColor]] selectedBackgroundImage:nil];
    closeButton.layer.masksToBounds = YES;
    closeButton.layer.cornerRadius = 5.0f;
    closeButton.frame =CGRectMake(screenWidth*0.75f, screenHeight*0.1f,70.0f, 40.0f);
    [bgView addSubview:closeButton];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:self.containImage];
    imageView.frame = CGRectMake(screenWidth*0.1, screenHeight*0.2, screenWidth*0.8, screenHeight*0.6);
    imageView.alpha = 1.0f;
    [bgView addSubview:imageView];
    
    [closeButton bk_whenTapped:^{
        bgView.frame = CGRectMake(-screenWidth, 0, screenWidth, screenHeight);
    }];
    [[UIApplication sharedApplication].keyWindow addSubview:bgView];
}
@end
