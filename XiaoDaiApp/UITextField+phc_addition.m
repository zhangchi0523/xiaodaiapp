//
//  UITextField+phc_addition.m
//  p2p
//
//  Created by Philip on 9/6/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UITextField+phc_addition.h"
#import "UIFont+phc_addition.h"
#import "UIView+phc_addition.h"
#import "UIColor+phc_addition.h"

@implementation UITextField (phc_addition)

+ (instancetype)phc_textFieldWithFontSize:(CGFloat)size textColor:(UIColor *)color placeholderText:(NSString *)placeholder text:(NSString *)text {
    UITextField *textField = [[self alloc] init];
    textField.font = [UIFont phc_defaultFontWithSize:size];
    textField.textColor = color;
    textField.placeholder = placeholder;
    textField.text = text;
    return textField;
}

+ (instancetype)phc_defaultFieldWithPlaceholderText:(NSString *)placeholderText text:(NSString *)text {
    UITextField *textField = [self phc_textFieldWithFontSize:15.0f textColor:[UIColor whiteColor] placeholderText:placeholderText text:text];
    [textField setValue:[UIColor phc_lightBlue] forKeyPath:@"_placeholderLabel.textColor"];
    return textField;
}

@end
