//
//  PHTableViewDataSource.m
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHMessage.h"
#import <Bolts/Bolts.h>

@interface PHTableViewDataSource() <UITableViewDataSource>
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, copy) NSArray<NSMutableArray<PHTableViewModel *> *> *sections;
@property (nonatomic, strong) NSOperationQueue *renderQueue;
@property (nonatomic, assign, getter=shouldShowIndexTitle) BOOL showIndexTitle;
@end

@implementation PHTableViewDataSource

- (instancetype)initWithTableView:(UITableView *)tableView {
    return [self initWithTableView:tableView showIndexTitle:YES];
}

- (instancetype)initWithTableView:(UITableView *)tableView showIndexTitle:(BOOL)shouldShowIndexTitle {
    if (self = [super init]) {
        _showIndexTitle = shouldShowIndexTitle;
        _renderQueue = [NSOperationQueue new];
        _renderQueue.maxConcurrentOperationCount = 1;
        _tableView = tableView;
        _tableView.estimatedRowHeight = 65.0f;
        _tableView.dataSource = self;
        [_tableView registerClass:UITableViewCell.class forCellReuseIdentifier:NSStringFromClass(UITableViewCell.class)];
        
        NSMutableArray *mutableSections = [NSMutableArray new];
        for (NSUInteger index = 0; index < [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count]; index ++) {
            [mutableSections addObject:[NSMutableArray new]];
        }
        _sections = [mutableSections copy];
    }
    
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger section = self.shouldShowIndexTitle ? indexPath.section : 0;
    
    NSArray *sectionItems = self.sections[section];
    
    if (indexPath.row >= sectionItems.count) return nil;
    return sectionItems[indexPath.row];
}

- (void)addItems:(NSArray<PHTableViewModel *> *)items {
    typeof(self) __weak weakSelf = self;
    
    [[[BFTask taskFromExecutor:[BFExecutor executorWithOperationQueue:self.renderQueue] withBlock:^id{
        [NSThread sleepForTimeInterval:0.05f];
        return [BFTask taskWithResult:@YES];
    }] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        NSMutableArray *addedIndexPath = [NSMutableArray new];
        
        for (PHTableViewModel *item in items) {
            [weakSelf.tableView registerClass:item.cellClass forCellReuseIdentifier:NSStringFromClass(item.cellClass)];
            NSInteger sectionNumber = weakSelf.shouldShowIndexTitle ? [[UILocalizedIndexedCollation currentCollation] sectionForObject:item.item collationStringSelector:item.collationStringSelector] : 0;
            NSMutableArray *sectionItems = weakSelf.sections[sectionNumber];
            NSUInteger insertIndex = sectionItems.count;
            
            [sectionItems addObject:item];
            [addedIndexPath addObject:[NSIndexPath indexPathForRow:insertIndex inSection:sectionNumber]];
        }
        
        return [BFTask taskWithResult:addedIndexPath];
    }] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        [weakSelf.tableView reloadData];
        return nil;
    }];
}

- (void)removeAllItems {
    typeof(self) __weak weakSelf = self;
    
    [[BFTask taskFromExecutor:[BFExecutor executorWithOperationQueue:self.renderQueue] withBlock:^id{
        [NSThread sleepForTimeInterval:0.05f];
        return [BFTask taskWithResult:@YES];
    }] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        for (NSMutableArray *section in weakSelf.sections) {
            [section removeAllObjects];
        }
        
        [weakSelf.tableView reloadData];
        return nil;
    }];
}

- (void)reloadData {
    typeof(self) __weak weakSelf = self;
    
    [[BFTask taskFromExecutor:[BFExecutor executorWithOperationQueue:self.renderQueue] withBlock:^id{
        [NSThread sleepForTimeInterval:0.05f];
        return [BFTask taskWithResult:@YES];
    }] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        [weakSelf.tableView reloadData];
        return nil;
    }];
}

- (void)reloadDataAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
    typeof(self) __weak weakSelf = self;
    
    [[BFTask taskFromExecutor:[BFExecutor executorWithOperationQueue:self.renderQueue] withBlock:^id{
        [NSThread sleepForTimeInterval:0.15f];
        return [BFTask taskWithResult:@YES];
    }] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        [weakSelf.tableView beginUpdates];
        [weakSelf.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.tableView endUpdates];
        return nil;
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionItems = self.sections[section];
    return sectionItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger section = self.shouldShowIndexTitle ? indexPath.section : 0;
    NSArray *sectionItems = self.sections[section];
    
    if (indexPath.row >= sectionItems.count) return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(UITableViewCell.class)];
    
    PHTableViewModel *model = sectionItems[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(model.cellClass) forIndexPath:indexPath];
    
    if (model.configurationBlock) {        
        model.configurationBlock(cell, model.item);
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (!self.shouldShowIndexTitle) return nil;
    
    NSArray *sectionItems = self.sections[section];
    
    return sectionItems.count ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : @"";
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (!self.shouldShowIndexTitle) return nil;
    
    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (!self.shouldShowIndexTitle) return 0;
    
    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}

- (void)clearRenderQueue {
    [self.renderQueue cancelAllOperations];
}

@end
