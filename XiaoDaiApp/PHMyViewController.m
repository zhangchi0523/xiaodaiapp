//
//  PHMyViewController.m
//  XiaoDaiApp
//
//  Created by 邹彦军 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHMyViewController.h"
#import "UIColor+phc_addition.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import "UIControl+BlocksKit.h"
#import "PHLoanListViewController.h"
static NSString *cellIdentifier = @"Cell";
static NSInteger buttonHeight = 44;
static NSInteger headerViewHeight = 150;
static NSInteger buttonMargin = 15;
static NSInteger buttonLeftMargin = 30;

@interface PHMyViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *exitButton;
@property (nonatomic ,strong) NSArray *listArray;
@property (nonatomic ,strong) NSArray *buttonArray;
@property (nonatomic ,strong) NSString *channelNumberString;  //渠道编号

@end

@implementation PHMyViewController

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"我的";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"personalCenter_icon"] selectedImage:nil];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(5.0f, 0.0f, -5.0f, 0.0f);    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.channelNumberString = @"1000";
    
    self.listArray = [NSArray arrayWithObjects:
                      NSLocalizedString(@"changePassword", nil),
                      NSLocalizedString(@"identityAuthentication", nil),NSLocalizedString(@"applicationsManager", nil),nil
                      ];
    
    self.buttonArray = [NSArray arrayWithObjects:
                      NSLocalizedString(@"exitTitle", nil),nil
                      ];

    [self setupUI];
}

- (void)setupUI {
    
    self.navigationController.navigationBar.barTintColor = [UIColor blueColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor phc_lightGray];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = [self createHeaderView];
    self.tableView.tableFooterView = [self createFooterView];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.view addSubview:self.tableView];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    cell.imageView.image = [UIImage imageNamed:@"menu_item_1"];
    cell.textLabel.text = self.listArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            
            break;
            
        case 1:
            
            break;
        case 2:
            [self.navigationController pushViewController:[PHLoanListViewController new] animated:YES];
            break;


        default:
            break;
    }
}

- (UIView *)createHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,headerViewHeight)];
    view.backgroundColor = [UIColor grayColor];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_item_1"]];
    [view addSubview:imageView];
    
    UILabel * channelNumberLabel = [UILabel new];
    channelNumberLabel.text = [NSString stringWithFormat:@"渠道编号:%@",self.channelNumberString];
    channelNumberLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [view addSubview:channelNumberLabel];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(view);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    [channelNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(5);
        make.left.equalTo(imageView);
    }];
    return view;
}

- (UIView *)createFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, headerViewHeight)];

    NSString *string;
    for (int i = 0; i < self.buttonArray.count; i++) {
        string = [self.buttonArray objectAtIndex:i];
        [self createButtonWithFrame:CGRectMake(buttonLeftMargin, (buttonMargin + buttonHeight) * i + buttonHeight * i, self.view.frame.size.width - buttonLeftMargin * 2, buttonHeight) setTitle:string setTag:i+1 addSubview:view backgroundColor:[UIColor redColor]];
    }
    return view;
}

- (UIButton *)createButtonWithFrame:(CGRect)rect setTitle:(NSString *)title setTag:(NSInteger)tag addSubview:(UIView *)view backgroundColor:(UIColor *)backgroundColor {
    UIButton *button = [[UIButton alloc]initWithFrame:rect];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(exitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = backgroundColor;
    button.layer.cornerRadius = 5;
    button.tag = tag;
    [view addSubview:button];
    return button;
}

- (void)exitButtonClick:(UIButton *)button {
    NSLog(@"退出按钮点击");
}

@end
