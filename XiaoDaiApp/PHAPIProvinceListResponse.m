//
//  PHAPIProvinceListResponse.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIProvinceListResponse.h"
#import "PHParsingJSONDictionary.h"

@implementation PHAPIProvinceListResponse

//+ (NSValueTransformer *)userJSONTransformer {
//    return [MTLJSONAdapter dictionaryTransformerWithModelClass:PHParsingJSONDictionary.class];
//}
//+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key {
//    
//    
//    return nil;
//}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[NSDictionary mtl_identityPropertyMapWithModel:self] mutableCopy];
    mapping[@"ProvinceId"] = @"ProvinceId";
    mapping[@"ProvinceName"] = @"ProvinceName";
    return mapping;
}

@end
