//
//  PHSubmitInformationViewController.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHSubmitInformationViewController.h"
#import "UIColor+phc_addition.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHTextFieldTableViewCell.h"
#import "PHKeyValueTableViewCell.h"
#import "UIControl+BlocksKit.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import "UILabel+phc_addition.h"
#import "UIView+BlocksKit.h"
#import "PHRegisterViewController.h"
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHTextFieldTableViewCellTwo.h"
#import "PHUpdatePictureViewController.h"
#import "PHApplications.h"
@interface PHSubmitInformationViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) PHObjectManager *objectManager;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *typeText;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *idNum;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *term;
@property (nonatomic, strong) UIColor *typeTextColor;
@property (nonatomic, strong) UIColor *termTextColor;


@end

@implementation PHSubmitInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.typeTextColor = [UIColor phc_gray];
    self.termTextColor = [UIColor phc_gray];
    self.typeText = @"房屋抵押(一抵)";
    self.term = @"12月";
    self.objectManager = [PHObjectManager sharedInstance];
    self.view.backgroundColor = [UIColor phc_lightGray];
    self.title = @"提交信息";
    typeof(self) __weak weakSelf = self;
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"姓名" placeholder:@"请输入姓名" text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.name = textField.text;
                               c.textField.delegate = weakSelf;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"身份证" placeholder:@"请输入身份证" text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.idNum = textField.text;
                               c.textField.delegate = weakSelf;

                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"联系方式" placeholder:@"请输入手机号" text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.phone = textField.text;
                               c.textField.delegate = weakSelf;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"贷款类型" details:weakSelf.typeText textColor:weakSelf.typeTextColor];
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCellTwo.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCellTwo *c = (PHTextFieldTableViewCellTwo *)cell;
                           [c configureWithTitle:@"贷款金额" placeholder:@"请输入贷款金额" text:nil rightText:@"万元"];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.amount = [NSString stringWithFormat:@"%.2f",textField.text.floatValue*10000];
                               c.textField.delegate = weakSelf;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"贷款期限" details:weakSelf.term textColor:weakSelf.termTextColor];
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       ];
    
    [self.dataSource addItems:items];
    [self addObserver:self forKeyPath:@"typeText" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"idNum" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"phone" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"term" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"amount" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self setupFooterView];
}

- (void)setupFooterView{
    typeof(self) __weak weakSelf = self;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];
    self.nextButton = [UIButton phc_actionButtonWithTitle:@"下一步"];
    self.nextButton.enabled = NO;
    [footerView addSubview:self.nextButton];
    self.tableView.tableFooterView = footerView;
    
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(footerView);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.9f);
        make.height.equalTo(@40);
    }];
    
    [self.nextButton bk_addEventHandler:^(UIButton *sender) {
        [weakSelf.view endEditing:YES];
        PHUpdatePictureViewController *updatePictureVC = [[PHUpdatePictureViewController alloc]initWithName:weakSelf.name withPhone:weakSelf.phone withIdNumber:weakSelf.idNum withType:weakSelf.type withAccount:weakSelf.amount withValuationId:weakSelf.valuationId withAddress:weakSelf.address withTerm:weakSelf.term];
        [weakSelf.navigationController pushViewController:updatePictureVC animated:YES];
        
        
    } forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = footerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    typeof(self) __weak weakSelf = self;
    if (indexPath.row == 5) {
        NSArray *typeArray = @[@"房屋抵押(一抵)",@"房屋抵押(二抵)"];
        NSDictionary *typeDic = @{@"房屋抵押(一抵)":@"LOAN_TYPE_1",@"房屋抵押(二抵)":@"LOAN_TYPE_2"};
        [[self taskToShowPickerDataArray:typeArray]continueWithBlock:^id(BFTask *task) {
            weakSelf.typeTextColor = [UIColor blackColor];
            weakSelf.typeText = task.result;
            weakSelf.type = [typeDic objectForKey:task.result];
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]]];

            return task;
        }];
    }else if (indexPath.row == 7){
        NSArray *termArray = @[@"3月",@"6月",@"9月",@"12月"];
        [[self taskToShowPickerDataArray:termArray]continueWithBlock:^id(BFTask *task) {
            weakSelf.termTextColor = [UIColor blackColor];
            weakSelf.term = task.result;
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:7 inSection:0]]];
            
            return task;
        }];
    }

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    self.nextButton.enabled = (self.name.length && self.idNum.length && self.phone.length && self.typeText.length&&self.amount&&self.term);
}
- (void)dealloc {
    [self removeObserver:self forKeyPath:@"name"];
    [self removeObserver:self forKeyPath:@"typeText"];
    [self removeObserver:self forKeyPath:@"idNum"];
    [self removeObserver:self forKeyPath:@"phone"];
    [self removeObserver:self forKeyPath:@"term"];
    [self removeObserver:self forKeyPath:@"amount"];

    
}
@end
