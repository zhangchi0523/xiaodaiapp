//
//  PHAPIGetBranchResponse.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/6.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIGetBranchResponse.h"
@interface PHAPIGetBranchResponse ()

@property(nonatomic, readwrite, copy) NSURL* request;
@property(nonatomic, readwrite, copy) NSString* message;
@property(nonatomic, readwrite, copy) NSArray* data;
@property(nonatomic, readwrite, assign) NSInteger errorCode;
@property(nonatomic, readwrite, assign) NSInteger code;

@end

@implementation PHAPIGetBranchResponse
+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"code":@"status.code",
             @"message":@"status.message",
             @"data":@"data",
             };
}

+ (NSValueTransformer *)requestJSONTransformer{
    return [MTLValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}
@end
