//
//  PHKeyValueTableViewCell.m
//  p2p
//
//  Created by Philip on 10/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHMenuTableViewCell.h"
#import "UILabel+phc_addition.h"
#import "UIFont+phc_addition.h"
#import "UIColor+phc_addition.h"
#import "UIImage+phc_addition.h"
#import <Masonry/Masonry.h>

@interface PHMenuTableViewCell()
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, strong) UIView *seperater;
@property (nonatomic,strong)UIImageView *leftImageView;

@end

@implementation PHMenuTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _leftImageView = [[UIImageView alloc]initWithImage:[UIImage phc_imageWithColor:[UIColor blackColor]]];
        [self.contentView addSubview:_leftImageView];
        _leftLabel= [UILabel phc_labelWithFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
        _leftLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_leftLabel];
        _rightLabel = [UILabel phc_labelWithFontSize:12.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:1];
        _rightLabel.textAlignment = NSTextAlignmentRight;
        _rightLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_rightLabel];
        
        _seperater = [UIView new];
        _seperater.backgroundColor = [UIColor phc_gray];
        [self.contentView addSubview:_seperater];
        
        typeof(self) __weak weakSelf = self;
        [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(6.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-4.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(@(28));
            

        }];
        
        [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.leftImageView.mas_right).with.offset(10.0f);
            make.width.equalTo(weakSelf.mas_width).with.multipliedBy(0.4f);
        }];
        
        [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.contentView.mas_right).with.offset(-5.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.left.equalTo(_leftLabel.mas_right);
        }];
        
        [_seperater mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.contentView);
            make.width.equalTo(weakSelf.contentView);
            make.height.equalTo(@1);
        }];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.leftLabel.text = nil;
    self.rightLabel.text = nil;
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details withDetailsFont:(NSInteger)detailsFont withDetailsColor:(UIColor *)detailsColor  withLeftImage:(UIImage *)image{
    [self configureWithTitle:title details:details rightAlignmentMode:NO withLeftImage:image detailsFont:detailsFont detailsColor:detailsColor];
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details withLeftImage:(UIImage *)image{
    [self configureWithTitle:title details:details rightAlignmentMode:NO withLeftImage:image detailsFont:12 detailsColor:[UIColor blackColor]];
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details rightAlignmentMode:(BOOL)rightAlignmentMode withLeftImage:(UIImage *)image detailsFont:(NSInteger)detailsFont detailsColor:(UIColor *)detailsColor{
    [self.seperater setHidden:YES];
    self.leftLabel.text = title;
    self.rightLabel.text = details;
    self.leftImageView.image = image;
    
    self.rightLabel.textColor = detailsColor;
    
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (rightAlignmentMode) {
        typeof(self) __weak weakSelf = self;
        [self.leftLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.mas_width).with.multipliedBy(0.25f);
        }];
        
        self.leftLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)showLine {
    [self.seperater setHidden:NO];
}

@end
