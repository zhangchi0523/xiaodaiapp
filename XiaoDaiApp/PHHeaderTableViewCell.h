//
//  PHHeaderTableViewCell.h
//  p2p
//
//  Created by Philip on 10/21/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTableViewCell.h"

@interface PHHeaderTableViewCell : PHTableViewCell

@end
