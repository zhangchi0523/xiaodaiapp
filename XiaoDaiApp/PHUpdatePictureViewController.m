//
//  PHUpdatePictureViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/16.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHUpdatePictureViewController.h"
#import <Masonry.h>
#import "UIColor+phc_addition.h"
#import "UILabel+phc_addition.h"
#import "UIImage+phc_addition.h"
#import "UIButton+phc_addition.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import <AliyunOSSiOS/OSSService.h>
#import "PHApplications.h"
#import "PHObjectManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHUserProfile.h"
#define IMAGE_NECESSARY_COUNT 5

@interface PHUpdatePictureViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong)UICollectionView *containerListViewOne;
@property(nonatomic,strong)UIButton *verifyButton;
@property(nonatomic,strong)UICollectionView *collectionListViewTwo;
@property(nonatomic,strong)NSMutableArray *pictureArray;
@property(nonatomic,strong)NSMutableArray *pictureDefaultAarray;
@property(nonatomic,strong)NSMutableArray *pictureDataAarray;
@property(nonatomic,assign)NSInteger pictureIndex;
@property(nonatomic,strong)OSSClient *client;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) PHUserProfile *userProfile;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *phone;
@property(nonatomic,strong)NSString *idNum;
@property(nonatomic,strong)NSString *type;
@property(nonatomic,strong)NSString *account;
@property(nonatomic,strong)NSString *valuationId;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSString *term;
@property(nonatomic,strong)NSMutableArray *pictureNameArray;
@property(nonatomic,strong)NSMutableArray *pictureUrlArray;


@end

@implementation PHUpdatePictureViewController
- (instancetype)initWithName:(NSString*)name withPhone:(NSString *)phone withIdNumber:(NSString *)idNum withType:(NSString *)type withAccount:(NSString *)account withValuationId:(NSString *)valuationId withAddress:(NSString *)address withTerm:(NSString *)term;
{
    if (self = [super init]) {
        self.navigationItem.title = NSLocalizedString(@"uploadPicture", nil);
        _name = [name copy];
        _phone =[phone copy];
        _idNum =[idNum copy];
        _type =[type copy];
        _account =[account copy];
        _valuationId =[valuationId copy];
        _address = [address copy];
        _term = [term copy];
        _pictureArray = [[NSMutableArray alloc]initWithCapacity:0];
        _pictureDefaultAarray =[[NSMutableArray alloc]initWithCapacity:0];
        _pictureDataAarray = [[NSMutableArray alloc]initWithCapacity:0];
        _pictureIndex = 0;
        _userProfile = [[NSUserDefaults standardUserDefaults] objectForKey:@"kAuthUserKey"];
        UIImage *image = [UIImage imageNamed:@"identificationFront"];
        UIImage *image1 = [UIImage imageNamed:@"identificationBack"];
        UIImage *image2 = [UIImage imageNamed:@"hukouFront"];
        UIImage *image3 = [UIImage imageNamed:@"hukouBack"];
        UIImage *image4 = [UIImage imageNamed:@"deed"];
        UIImage *image5 = [UIImage imageNamed:@"figure"];
        [self.pictureDefaultAarray addObjectsFromArray:@[image,image1,image2,image3,image4,image5]];
        [self.pictureArray addObjectsFromArray:self.pictureDefaultAarray];
        [self.pictureDataAarray addObjectsFromArray:self.pictureDefaultAarray];
        
        NSArray *array = @[[NSString stringWithFormat:@"%@%@",self.phone,@"identificationFront.png"],[NSString stringWithFormat:@"%@%@",self.phone,@"identificationBack.png"],[NSString stringWithFormat:@"%@%@",self.phone,@"hukouFront.png"],[NSString stringWithFormat:@"%@%@",self.phone,@"hukouBack.png"],[NSString stringWithFormat:@"%@%@",self.phone,@"deed.png"]];
        NSArray *arrayUrl = @[[NSString stringWithFormat:@"http://%@.%@/%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,@"identificationFront.png"],[NSString stringWithFormat:@"http://%@.%@/%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,@"identificationBack.png"],[NSString stringWithFormat:@"http://%@.%@/%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,@"hukouFront.png"],[NSString stringWithFormat:@"http://%@.%@/%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,@"hukouBack.png"],[NSString stringWithFormat:@"http://%@.%@/%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,@"deed.png"]];
        _pictureNameArray = [NSMutableArray arrayWithArray:array];
        _pictureUrlArray = [NSMutableArray arrayWithArray:arrayUrl];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor phc_lightGray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chosePictureSuccess:) name:PHNotificationUploadPicture object:nil];
    [self addObserver:self forKeyPath:@"pictureIndex" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self setUpUI];
}
-(void)viewWillAppear:(BOOL)animated{
    if ([self checkPicture]) {
        self.verifyButton.enabled = YES;
    }
}
-(void)setUpUI{
    typeof(self) __weak weakSelf = self;
    UIView *containerViewOne = [UIView new];
    [self.view addSubview:containerViewOne];
    containerViewOne.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabelComponentOneRowOne = [UILabel phc_labelWithBoldFontSize:18.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
    titleLabelComponentOneRowOne.text = NSLocalizedString(@"uploadNecessaryPicture", nil);
    [containerViewOne addSubview:titleLabelComponentOneRowOne];
    UILabel *titleLabelComponentOneRowTwo = [UILabel phc_labelWithFontSize:14.0f textColor:[UIColor grayColor] backgroundColor:nil numberOfLines:0];
    titleLabelComponentOneRowTwo.text =NSLocalizedString(@"idNumAndHouseAndRegister", nil);
    [containerViewOne addSubview:titleLabelComponentOneRowTwo];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    _containerListViewOne= [[UICollectionView alloc]initWithFrame:CGRectMake(0,0,0,0) collectionViewLayout:layout];
    [containerViewOne addSubview:self.containerListViewOne];
    self.containerListViewOne.backgroundColor = [UIColor whiteColor];
    [self.containerListViewOne registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.containerListViewOne.delegate = self;
    self.containerListViewOne.dataSource = self;
    
    UIView *containerViewTwo = [UIView new];
    containerViewTwo.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerViewTwo];
    UILabel *titleLabelComponentTwoRowOne = [UILabel phc_labelWithBoldFontSize:18.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
    titleLabelComponentTwoRowOne.text = NSLocalizedString(@"uploadOptionalPicture", nil);
    [containerViewTwo addSubview:titleLabelComponentTwoRowOne];
    UILabel *titleLabelComponentTwoRowTwo = [UILabel phc_labelWithFontSize:14.0f textColor:[UIColor grayColor] backgroundColor:nil numberOfLines:0];
    titleLabelComponentTwoRowTwo.text =NSLocalizedString(@"marriageAndWealthAndCreditCertificate", nil);
    [containerViewTwo addSubview:titleLabelComponentTwoRowTwo];
    
    UICollectionViewFlowLayout *layoutTwo = [[UICollectionViewFlowLayout alloc] init];
    [layoutTwo setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _collectionListViewTwo= [[UICollectionView alloc]initWithFrame:CGRectMake(0,0,0,0) collectionViewLayout:layoutTwo];
    self.collectionListViewTwo.backgroundColor = [UIColor whiteColor];
    [containerViewTwo addSubview:self.collectionListViewTwo];
    [self.collectionListViewTwo registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionListViewTwo.delegate = self;
    self.collectionListViewTwo.dataSource = self;
    
    _verifyButton = [UIButton phc_actionButtonWithTitle:NSLocalizedString(@"uploadApply", nil)];
    self.verifyButton.enabled = NO;
    [self.view addSubview:self.verifyButton];
    [self.verifyButton bk_whenTapped:^{
        if ([self checkPicture]) {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self.pictureDataAarray removeLastObject];
            if (self.pictureDataAarray.count>IMAGE_NECESSARY_COUNT) {
                for (NSInteger i=0; i<self.pictureDataAarray.count-IMAGE_NECESSARY_COUNT; i++) {
                    [weakSelf.pictureNameArray addObject:[NSString stringWithFormat:@"%@_otherMedia%d.png",self.phone,i]];
                }
            }
            [[weakSelf taskToUploadPictureToAliOSSWithData:self.pictureDataAarray withNameArray:weakSelf.pictureNameArray] continueWithBlock:^id _Nullable(BFTask * _Nonnull task){
                if ([task.result isEqual:@YES]) {
                    NSMutableArray *array = [[NSMutableArray alloc]initWithCapacity:0];
                    for (NSInteger i= 0; i<weakSelf.pictureNameArray.count-IMAGE_NECESSARY_COUNT; i++) {
                        [array addObject:[NSString stringWithFormat:@"http://%@.%@/%@_otherMedia%d.png",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBucketName"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"PHUploadBaseURL"],self.phone,i]];
                    }
                    PHApplications *app = [[PHApplications alloc]initWithName:weakSelf.name withAmount:weakSelf.account withPhone:weakSelf.phone withIdentificationNumber:weakSelf.idNum withidentificationFront:[weakSelf.pictureUrlArray objectAtIndex:0] withidentificationBack:[weakSelf.pictureUrlArray objectAtIndex:1] withHukouFront:[weakSelf.pictureUrlArray objectAtIndex:2] withHukouBack:[weakSelf.pictureUrlArray objectAtIndex:3] withDeed:[weakSelf.pictureUrlArray objectAtIndex:4] withType:weakSelf.type withValuationId:weakSelf.valuationId withAddress:@"121" withImageArray:array];
                    [[[PHObjectManager sharedInstance]taskToApplications:app]continueWithBlock:^id _Nullable(BFTask * _Nonnull task){
                        [weakSelf.hud hideAnimated:YES];
                        if (task.error) {
                            [weakSelf taskToShowError:task.error];
                        }else{
                            [[weakSelf taskToShowPrompt:@"申请成功"] continueWithSuccessBlock:^id _Nullable(BFTask * _Nonnull task){
                                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                return task;
                            }];
                        }
                        return task;
                    }];
                }
                return task;
            }];
        }else{
            [self taskToShowPrompt:@"图片未添加完全"];
        }
    }];
    [containerViewOne mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(weakSelf.view.mas_top).with.offset(80.0f);
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.height.equalTo(weakSelf.view.mas_height).with.multipliedBy(0.4);
    }];
    [titleLabelComponentOneRowOne mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(containerViewOne.mas_top).with.offset(10.0f);
        make.left.equalTo(containerViewOne.mas_left).with.offset(10.0f);
        make.height.equalTo(@(18));
        make.width.equalTo(@(110));
    }];
    [titleLabelComponentOneRowTwo mas_makeConstraints:^(MASConstraintMaker *make){
        make.bottom.equalTo(titleLabelComponentOneRowOne.mas_bottom);
        make.left.equalTo(titleLabelComponentOneRowOne.mas_right);
        make.height.equalTo(@(16));
        make.width.equalTo(@(200));
    }];
    [self.containerListViewOne mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(titleLabelComponentOneRowOne.mas_bottom).with.offset(10.0f);
        make.left.equalTo(weakSelf.view.mas_left).with.offset(10.0f);
        make.right.equalTo(weakSelf.view.mas_right).with.offset(-10.0f);
        make.bottom.equalTo(containerViewOne.mas_bottom);
    }];
    
    [containerViewTwo mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(containerViewOne.mas_bottom).with.offset(10.0f);
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.height.equalTo(weakSelf.view.mas_height).with.multipliedBy(0.22);
    }];
    [titleLabelComponentTwoRowOne mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(containerViewTwo.mas_top).with.offset(10.0f);
        make.left.equalTo(containerViewTwo.mas_left).with.offset(10.0f);
        make.height.equalTo(@(18));
        make.width.equalTo(@(110));
    }];
    [titleLabelComponentTwoRowTwo mas_makeConstraints:^(MASConstraintMaker *make){
        make.bottom.equalTo(titleLabelComponentTwoRowOne.mas_bottom);
        make.left.equalTo(titleLabelComponentTwoRowOne.mas_right);
        make.height.equalTo(@(16));
        make.width.equalTo(@(230));
    }];
    [self.collectionListViewTwo mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(titleLabelComponentTwoRowOne.mas_bottom).with.offset(10.0f);
        make.left.equalTo(weakSelf.view.mas_left).with.offset(10.0f);
        make.right.equalTo(weakSelf.view.mas_right).with.offset(-10.0f);
        make.bottom.equalTo(containerViewTwo.mas_bottom);
    }];
    [self.verifyButton mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(containerViewTwo.mas_bottom).with.offset(20.0f);
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.width.equalTo(weakSelf.view.mas_width).with.multipliedBy(0.9);
        make.height.equalTo(@(40));
    }];
}

-(BOOL)checkPicture{
    for (int i=0;i<self.pictureArray.count-1;i++) {
        UIImage *image = [self.pictureArray objectAtIndex:i];
        UIImage  *defaultImage = [self.pictureDefaultAarray objectAtIndex:i];
        if (image == defaultImage ) {
            return NO;
        }
    }
    return YES;
}
-(void)chosePictureSuccess:(NSNotification *)sender{
    if ([self checkPicture]) {
        self.verifyButton.enabled = YES;
    }
    UIImage *image = [sender.userInfo objectForKey:@"image"];
    NSData *data = UIImageJPEGRepresentation(image, 1);
    [self.pictureDataAarray replaceObjectAtIndex:self.pictureIndex withObject:data];
    [self.pictureArray replaceObjectAtIndex:self.pictureIndex withObject:image];
    if (self.pictureIndex<IMAGE_NECESSARY_COUNT) {
        [self.containerListViewOne reloadData];
    }else{
        UIImage *image = [UIImage imageNamed:@"figure"];
        [self.pictureArray addObject:image];
        [self.pictureDataAarray addObject:image];
        [self.pictureDefaultAarray addObject:image];
        [self.collectionListViewTwo reloadData];
    }
}

#pragma mark collectionView代理方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView == self.containerListViewOne){
        return 2;
    }else{
        return (self.pictureArray.count-IMAGE_NECESSARY_COUNT)/3+1;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionListViewTwo&&section==((self.pictureArray.count-IMAGE_NECESSARY_COUNT)/3)) {
        return ((self.pictureArray.count-IMAGE_NECESSARY_COUNT)%3);
    }else{
        return 3;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = nil;
    if (collectionView == self.containerListViewOne) {
        if (indexPath.section==1&&indexPath.row==2) {
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            return cell;
        }
        imageView = [[UIImageView alloc]initWithImage:[self.pictureArray objectAtIndex:indexPath.section*3+indexPath.row]];
        
    }else{
        imageView = [[UIImageView alloc]initWithImage:[self.pictureArray objectAtIndex:indexPath.section*3+indexPath.row+5]];
        
    }
    [cell.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(cell.contentView.mas_top).with.offset(10.0f);
        make.left.equalTo(cell.contentView.mas_left);
        make.right.equalTo(cell.contentView.mas_right);
        make.bottom.equalTo(cell.contentView.mas_bottom).with.offset(-10.0f);
    }];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width*0.25,self.view.frame.size.width*0.3);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.containerListViewOne) {
        if (indexPath.section==1&&indexPath.row==2) {
            return;
        }
        self.pictureIndex =indexPath.section*3+indexPath.row;
    }else{
        self.pictureIndex = indexPath.section*3+indexPath.row+5;
    }
    if ([self.pictureArray objectAtIndex:self.pictureIndex] != [self.pictureDefaultAarray objectAtIndex:self.pictureIndex]) {
        [self deletePicture];
        return;
    }
    [self choseAndTakePicture];
}

- (void)deletePicture {
    [[self taskToShowDetermineOrCancelPrompt:@"删除照片"]continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        if ([task.result isEqual:@YES]) {
            
            [self.pictureArray replaceObjectAtIndex:self.pictureIndex withObject:[self.pictureDefaultAarray objectAtIndex:self.pictureIndex]];
            if (self.pictureIndex<IMAGE_NECESSARY_COUNT) {
                [self.containerListViewOne reloadData];
            }else{
                [self.pictureArray removeObjectAtIndex:self.pictureIndex];
                [self.pictureDataAarray removeObjectAtIndex:self.pictureIndex];
                [self.pictureDefaultAarray removeObjectAtIndex:self.pictureIndex];
                [self.collectionListViewTwo reloadData];
                
            }
            if (![self checkPicture]) {
                self.verifyButton.enabled = NO;
            }
        }
        return task;
    }];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"pictureIndex"]) {
        if ([self checkPicture]) {
            self.verifyButton.enabled = YES;
        }
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObserver:self forKeyPath:@"pictureIndex"];

}
@end
