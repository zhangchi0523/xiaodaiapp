//
//  UITextField+phc_addition.h
//  p2p
//
//  Created by Philip on 9/6/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (phc_addition)

+ (instancetype)phc_textFieldWithFontSize:(CGFloat)size textColor:(UIColor *)color placeholderText:(NSString *)placeholder text:(NSString *)text;

+ (instancetype)phc_defaultFieldWithPlaceholderText:(NSString *)placeholderText text:(NSString *)text;

@end
