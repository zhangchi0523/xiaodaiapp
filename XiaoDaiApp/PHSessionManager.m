//
//  PHSessionManager.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHSessionManager.h"
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import <MTLJSONAdapter.h>
#import "PHUserProfile.h"
#import "PHAPILoginResponse.h"
#import "UIViewController+phc_addition.h"
#import <BlocksKit/BlocksKit+UIKit.h>

static NSString * const kAuthTokenKey = @"auth_token";
static NSString * const kAuthUsernameKey = @"auth_username";
static NSString * const kAuthPasswordKey = @"auth_password";
static NSString * const kAuthUserKey = @"auth_user";
NSString * const PHNotificationLogout = @"logoutNotification";

@interface PHSessionManager()

@property (nonatomic, weak) NSUserDefaults *userDefaults;
@property (nonatomic, weak) PHObjectManager *objectManager;
@property (nonatomic, assign, getter=isLoggedIn) BOOL loggedIn;
@property (nonatomic, copy, readwrite) NSString *token;
@property (nonatomic, copy, readwrite) NSString *username;
@property (nonatomic, copy, readwrite) NSString *password;
@property (nonatomic, strong, readwrite) PHUserProfile* currentUserProfile;

@end

@implementation PHSessionManager


+ (instancetype)sharedInstance {
    static PHSessionManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [PHSessionManager new];
    });
    
    return instance;
}

-  (instancetype)init {
    if (self = [super init]) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
        _loggedIn = NO;
        _user_token = [[_userDefaults objectForKey:kAuthTokenKey] copy];
        _username = [[_userDefaults objectForKey:kAuthUsernameKey] copy];
        _password = [[_userDefaults objectForKey:kAuthPasswordKey] copy];
        _currentUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData:[_userDefaults objectForKey:kAuthUserKey]];
        _loggedIn = (_token.length || (_username.length && _password.length));
        _objectManager = [PHObjectManager sharedInstance];
    }
    
    return self;
}
- (BFTask *)taskToPerformLogout {
    self.username = nil;
    self.password = nil;
    self.user_token = nil;
    self.currentUserProfile = nil;
    [self.userDefaults removeObjectForKey:kAuthTokenKey];
    [self.userDefaults removeObjectForKey:kAuthUsernameKey];
    [self.userDefaults removeObjectForKey:kAuthPasswordKey];
    [[NSNotificationCenter defaultCenter] postNotificationName:PHNotificationLogout object:nil userInfo:nil];

    return [BFTask taskWithResult:@YES];
}
- (BOOL)isLoggedIn {
    return (self.user_token.length);
}

@end
