//
//  PHLoan.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLoan.h"
@interface PHLoan()
@property (nonatomic, readwrite, copy) NSNumber *amount;
@property (nonatomic, readwrite, copy) NSNumber *duration;
@property (nonatomic, readwrite, copy) NSNumber *valuation;
@property (nonatomic, readwrite, copy) NSString *applicantName;
@property (nonatomic, readwrite, copy) NSString *status;

@property (nonatomic, readwrite, copy) NSString *applicantPhone;
@property (nonatomic, readwrite, copy) NSString *applicantIdentificationNumber;
@property (nonatomic, readwrite, copy) NSString *agentName;
@property (nonatomic, readwrite, copy) NSString *agentPhone;
@property (nonatomic, readwrite, copy) NSString *agentIdentificationNumber;
@property (nonatomic, readwrite, copy) NSString *address;
@property (nonatomic, readwrite, copy) NSString *type;

@property (nonatomic, readwrite, copy) NSNumber *buildArea;
@property (nonatomic, readwrite, copy) NSNumber *unitPrice;
@property (nonatomic, readwrite, copy) NSNumber *avagePrice;
@property (nonatomic, readwrite, copy) NSNumber *maxPrice;
@property (nonatomic, readwrite, copy) NSNumber *minPrice;
@property (nonatomic, readwrite, copy) NSNumber *appId;

@end
@implementation PHLoan
+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"appId":@"id",
             @"amount":@"amount",
             @"duration":@"duration",
             @"valuation":@"valuation.Amount",
             @"applicantName":@"applicant.name",
             @"status":@"status",
             @"applicantPhone":@"applicant.phone",
             @"applicantIdentificationNumber":@"applicant.identificationNumber",
             @"agentName":@"agent.name",
             @"agentPhone":@"agent.phone",
             @"agentIdentificationNumber":@"agent.phone",
             @"type":@"type",
             @"address":@"address",
             @"buildArea":@"valuation.BuildArea",
             @"unitPrice":@"valuation.UnitPrice",
             @"avagePrice":@"valuation.AvagePrice",
             @"maxPrice":@"valuation.MaxPrice",
             @"minPrice":@"valuation.MinPrice",


            };
}
@end
