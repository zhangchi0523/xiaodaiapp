//
//  PHViewController.h
//  yqrx
//
//  Created by 1234 on 15/11/25.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHViewController : UIViewController

- (NSString *)analyticsName;

@end
