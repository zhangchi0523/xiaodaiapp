//
//  UIViewController+phc_addition.h
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BFTask;
extern NSString * const PHImageName;
extern NSString * const PHNotificationUploadPicture;
@interface UIViewController (phc_addition)

- (BFTask *)taskToShowError:(NSError *)error;

- (BFTask *)taskToShowPrompt:(NSString *)prompt;

- (BFTask *)taskToShowDetermineOrCancelPrompt:(NSString *)prompt;
- (BFTask *)taskToUploadPictureToAliOSSWithData:(NSArray *)dataArray withNameArray:(NSArray *)nameArray;
-(void)choseAndTakePicture;
- (BFTask *)taskToShowPickerDataArray:(NSArray *)array;
- (NSString *)headerImagePath:(NSString *)imageName;
@end
