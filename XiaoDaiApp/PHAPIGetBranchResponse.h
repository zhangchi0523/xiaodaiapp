//
//  PHAPIGetBranchResponse.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/6.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PHAPIGetBranchResponse : MTLModel<MTLJSONSerializing>
@property(nonatomic, readonly, copy) NSURL* request;
@property(nonatomic, readonly, copy) NSString* message;
@property(nonatomic, readonly, copy) NSArray* data;
@property(nonatomic, readonly, assign) NSInteger errorCode;
@property(nonatomic, readonly, assign) NSInteger code;

@end
