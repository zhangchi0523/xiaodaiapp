//
//  UIButton+phc_addition.h
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (phc_addition)

+ (instancetype)phc_buttonWithFontSize:(CGFloat)size title:(NSString *)title textColor:(UIColor *)color backgroundImage:(UIImage *)backgroundImage selectedBackgroundImage:(UIImage *)selectedBackgroundImage;

+ (instancetype)phc_buttonWithFontSize:(CGFloat)size title:(NSString *)title textColor:(UIColor *)color selectedTextColor:(UIColor *)selectedColor titleEdgeInsets:(UIEdgeInsets)insets;

+ (instancetype)phc_actionButtonWithTitle:(NSString *)title;

@end
