//
//  APIResponse.m
//  p2p
//
//  Created by trila on 15/10/12.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import "PHAPIBaseResponse.h"

@interface PHAPIBaseResponse ()

@property(nonatomic, readwrite, copy) NSURL* request;
@property(nonatomic, readwrite, copy) NSString* message;
@property(nonatomic, readwrite, copy) NSDictionary* data;
@property(nonatomic, readwrite, assign) NSInteger errorCode;
@property(nonatomic, readwrite, assign) NSInteger code;

@end

@implementation PHAPIBaseResponse

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"code":@"status.code",
             @"message":@"status.message",
             @"data":@"data",
             };
}

+ (NSValueTransformer *)requestJSONTransformer{
    return [MTLValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
