//
//  PHTitleTableViewCell.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewCell.h"

@interface PHTitleTableViewCell : PHTableViewCell
@property (nonatomic, readonly, strong) UILabel *titleLabel;
- (void)configureWithTitle:(NSString *)title textColor:(UIColor *)color;

@end
