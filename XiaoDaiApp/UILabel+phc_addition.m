//
//  UILabel+phc_addition.m
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UILabel+phc_addition.h"
#import "UIFont+phc_addition.h"

@implementation UILabel (phc_addition)

+ (instancetype)phc_labelWithFontSize:(CGFloat)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor numberOfLines:(NSUInteger)numberOfLines bolded:(BOOL)bolded {
    UILabel *label = [UILabel new];
    label.font = (bolded) ? [UIFont phc_defaultBoldFontWithSize:size] : [UIFont phc_defaultFontWithSize:size];
    label.textColor = textColor;
    label.layer.backgroundColor = (backgroundColor.CGColor) ? : [UIColor clearColor].CGColor;
    label.numberOfLines = numberOfLines;
    [label setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    return label;
}

+ (instancetype)phc_labelWithFontSize:(CGFloat)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor numberOfLines:(NSUInteger)numberOfLines {
    return [self phc_labelWithFontSize:size textColor:textColor backgroundColor:backgroundColor numberOfLines:numberOfLines bolded:NO];
}

+ (instancetype)phc_labelWithBoldFontSize:(CGFloat)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor numberOfLines:(NSUInteger)numberOfLines {
    return [self phc_labelWithFontSize:size textColor:textColor backgroundColor:backgroundColor numberOfLines:numberOfLines bolded:YES];
}

@end
