//
//  PHValuationModel.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHValuationModel.h"

@interface PHValuationModel ()

@property (nonatomic, readwrite, copy) NSString *Note;
@property (nonatomic, readwrite, copy) NSNumber *Amount;
@property (nonatomic, readwrite, copy) NSNumber *AvagePrice;
@property (nonatomic, readwrite, copy) NSNumber *BuildArea;
@property (nonatomic, readwrite, copy) NSNumber *CaseCount;
@property (nonatomic, readwrite, copy) NSNumber *MaxPrice;
@property (nonatomic, readwrite, copy) NSNumber *MinPrice;
@property (nonatomic, readwrite, copy) NSNumber *Status;
@property (nonatomic, readwrite, copy) NSNumber *UnitPrice;
@property (nonatomic, readwrite, copy) NSNumber *number_id;

@end


@implementation PHValuationModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"Note":@"Note",
             @"Amount":@"Amount",
             @"AvagePrice":@"AvagePrice",
             @"BuildArea":@"BuildArea",
             @"CaseCount":@"CaseCount",
             @"MaxPrice":@"MaxPrice",
             @"MinPrice":@"MinPrice",
             @"Status":@"Status",
             @"UnitPrice":@"UnitPrice",
             @"number_id":@"id",
             };
}

+ (NSValueTransformer *)verifiedJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)isCreditDownJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
