//
//  PHObjectManager.h
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString *const ErrorDomain;
extern NSString *const HTTPErrorDomain;
@class  PHPaginator;
@class  BFTask;
@class PHApplications;
/**
 负责一切网络通讯等工作
 */
@interface PHObjectManager : NSObject

/**
 Singleton instance
 */
+ (instancetype)sharedInstance;

/**
 @brief 返回当前的根服务器地址
 */
- (NSURL *)baseURL;

/**
 @brief 登录
 */
- (BFTask *)taskToLoginUseName:(NSString *)useName passWord:(NSString *)passWord;

/**
 @brief 注册
 */
- (BFTask *)taskToRegisterAccountWithName:(NSString *)name PhoneNumber:(NSString *)phoneNumber password:(NSString *)password identificationNumber:(NSString *)identificationNumber withBranchId:(NSString *)branchId;
- (BFTask *)taskToProvincesList;
- (BFTask *)taskToCityList:(NSString *)provomces;
- (BFTask *)taskToCommunityList:(NSString *)city communityName:(NSString *)name;
- (BFTask *)taskToHouseList:(NSString *)city construction:(NSString *)construction;
- (BFTask *)taskToRoomList:(NSString *)city building:(NSString *)building;
- (BFTask *)taskToPriceCity:(NSString *)city construction:(NSString *)construction building:(NSString *)building room:(NSString *)room size:(NSString *)size;
-(BFTask *)taskToApplications:(PHApplications *)application;
-(BFTask *)taskToGetLoanList:(NSString *)page;
-(BFTask *)taskToGetLoanDetail:(NSString *)appId;
-(BFTask *)taskToGetBranch;
-(BFTask *)taskToChangePassword:(NSString *)password;
@end
