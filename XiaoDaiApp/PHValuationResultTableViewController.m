//
//  PHValuationResultTableViewController.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHValuationResultTableViewController.h"
#import "UIColor+phc_addition.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHTextFieldTableViewCell.h"
#import "PHKeyValueTableViewCell.h"
#import "UIControl+BlocksKit.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import "UILabel+phc_addition.h"
#import "UIView+BlocksKit.h"
#import "PHRegisterViewController.h"
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHSubmitInformationViewController.h"
#import "PHValuationModel.h"
#import "NSNumber+phc_addition.h"
@interface PHValuationResultTableViewController ()

@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) PHObjectManager *objectManager;

@end

@implementation PHValuationResultTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    typeof(self) __weak weakSelf = self;
    
    self.objectManager = [PHObjectManager sharedInstance];
    self.view.backgroundColor = [UIColor phc_lightGray];
    NSLog(@"%@",self.valuationModel.Note);
    self.title = @"估值结果";
    
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"最高可贷金额" details:[NSString stringWithFormat:@"%@ 元",[weakSelf.valuationModel.Amount currencyString]] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"评估价格" details:[NSString stringWithFormat:@"%@ 元",[weakSelf.valuationModel.AvagePrice currencyString]] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"最低单价" details:[NSString stringWithFormat:@"%@ /米",[weakSelf.valuationModel.MinPrice currencyString]] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"最高单价" details:[NSString stringWithFormat:@"%@ /米",[weakSelf.valuationModel.MaxPrice currencyString]] textColor:[UIColor blackColor]];
                       } collationStringSelector:nil],
                       ];
    [self.dataSource addItems:items];
    [self setupFooterView];
}

- (void)setupFooterView{
    typeof(self) __weak weakSelf = self;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];
    self.nextButton = [UIButton phc_actionButtonWithTitle:@"下一步"];
    [footerView addSubview:self.nextButton];
    self.tableView.tableFooterView = footerView;
    
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(footerView);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.9f);
        make.height.equalTo(@40);
    }];
    
    [self.nextButton bk_addEventHandler:^(UIButton *sender) {
        PHSubmitInformationViewController *submitVC = [PHSubmitInformationViewController new];
        submitVC.address = weakSelf.addressString;
        submitVC.amount = [NSString stringWithFormat:@"%d",weakSelf.valuationModel.Amount.integerValue];
        submitVC.valuationId = [NSString stringWithFormat:@"%d",weakSelf.valuationModel.number_id.integerValue];

        [weakSelf.navigationController pushViewController:submitVC animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = footerView;
}

@end
