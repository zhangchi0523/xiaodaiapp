//
//  PHLoanTableViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLoanTableViewController.h"
#import "PHTableViewModel.h"
#import "PHTableViewDataSource.h"
#import "PHTextFieldTableViewCell.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <Bolts/Bolts.h>
#import <Masonry/Masonry.h>
#import "UIButton+phc_addition.h"
#import "PHHeaderTableViewCell.h"
#import "UIColor+phc_addition.h"
#import "PHKeyValueTableViewCell.h"
#import "PHTextFieldTableViewCellTwo.h"
#import "PHValuationResultTableViewController.h"
#import "PHObjectManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIViewController+phc_addition.h"
#import "PHValuationModel.h"
#import "PHAPIValuationResponse.h"
#import "PHSearchViewController.h"
 NSString * const kProvinceListKey = @"ProvinceName";
 NSString * const kProvinceListID = @"ProvinceId";
 NSString * const kCityListKey = @"CityName";
 NSString * const kCityListID = @"CityId";
 NSString * const kCommunityListKey = @"ConArrea";
 NSString * const kCommunityListID = @"ConstructionId";
 NSString * const kHouseNumberListKey = @"BuildingName";
 NSString * const kHouseNumberListID = @"BuildingId";
 NSString * const kRoomListKey = @"HouseName";
 NSString * const kRoomListID = @"HouseId";

@interface PHLoanTableViewController ()<UITableViewDelegate>

@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIButton *verifyButton;
@property (nonatomic, strong) PHObjectManager *objectManager;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) NSArray *provinceListArray;
@property (nonatomic, strong) NSArray *cityListArray;
@property (nonatomic, strong) NSArray *communityListArray;
@property (nonatomic, strong) NSArray *houseNumberListArray;
@property (nonatomic, strong) NSArray *roomListArray;
@property (nonatomic, strong) NSString *provinceString;
@property (nonatomic, strong) NSString *cityString;
@property (nonatomic, strong) NSString *communityString;
@property (nonatomic, strong) NSString *houseNumberString;
@property (nonatomic, strong) NSString *roomString;
@property (nonatomic, strong) NSString *squareMetersString;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *titleColor;


@end

@implementation PHLoanTableViewController
- (instancetype)initWithProvinceListArray:(NSArray *)provinceListArray withProvinceString:(NSString *)provinceString withCityListArray:(NSArray *)cityListArray withCityString:(NSString *)cityString
{
    self = [super init];
    if (self) {
        self.navigationItem.title = @"估值";
        _provinceString = provinceString;
        _provinceListArray = [provinceListArray copy];
        _cityString = cityString;
        _cityListArray = [cityListArray copy];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.objectManager = [PHObjectManager sharedInstance];
    self.communityString = @"请选择小区";
    self.houseNumberString = @"请选择门牌号";
    self.roomString = @"请选择房间号";
    self.view.backgroundColor = [UIColor phc_lightGray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCommunity:) name:PHNotificationUpdatecommunity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateHouse:) name:PHNotificationUpdatehouse object:nil];


    [self setupTableView];
    [self setupFooterView];
}
- (void)setupTableView{
    self.tableView.delegate = self;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    self.textColor = [UIColor blackColor];
    self.titleColor = [UIColor blackColor];
    typeof(self) __weak weakSelf = self;
    
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"所在省份" details:self.provinceString textColor:self.textColor];
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"所在城市" details:self.cityString textColor:self.textColor];
                           [c setTitleTextColor:self.titleColor];
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           if (![self.communityString isEqualToString:@"请选择小区"]) {
                               [c configureWithTitle:@"小区列表" details:self.communityString textColor:self.textColor];
                               [c setTitleTextColor:self.titleColor];
                           }else{
                               [c configureWithTitle:@"小区列表" details:self.communityString textColor:[UIColor phc_newGray]];
                               [c setTitleTextColor:[UIColor phc_newGray]];
                           }
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           if (![self.houseNumberString isEqualToString:@"请选择门牌号"]) {
                               [c configureWithTitle:@"门牌号" details:self.houseNumberString textColor:self.textColor];
                               [c setTitleTextColor:self.titleColor];
                           }else{
                               [c configureWithTitle:@"门牌号" details:self.houseNumberString textColor:[UIColor phc_newGray]];
                               [c setTitleTextColor:[UIColor phc_newGray]];
                           }
                           

                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           if (![self.roomString isEqualToString:@"请选择房间号"]) {
                               [c configureWithTitle:@"房间号" details:self.roomString textColor:self.textColor];
                               [c setTitleTextColor:self.titleColor];
                           }else{
                               [c configureWithTitle:@"房间号" details:self.roomString textColor:[UIColor phc_newGray]];
                               [c setTitleTextColor:[UIColor phc_newGray]];
                           }
                           

                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCellTwo.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCellTwo *c = (PHTextFieldTableViewCellTwo *)cell;
                           
                           [c configureWithTitle:@"房屋面积" placeholder:@"请输入房屋面积" text:nil rightText:@"平米"];
                           [c setTitleTextColor:self.titleColor];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.squareMetersString = textField.text;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil]
                       ];
    [self.dataSource addItems:items];
    [self addObserver:self forKeyPath:@"provinceString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"cityString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"communityString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"houseNumberString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"roomString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"squareMetersString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
}
-(void)updateCommunity:(NSNotification *)sender{
    self.houseNumberString = @"请选择门牌号";
    self.roomString = @"请选择房间号";
    self.communityString = [sender.userInfo objectForKey:@"communityName"];
    self.communityListArray = [NSArray arrayWithArray:[sender.userInfo objectForKey:@"communityListArray"]];
    self.textColor = [UIColor blackColor];
    [self.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]]];
    [self.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]]];
    [self.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]]];

}
-(void)updateHouse:(NSNotification *)sender{
    self.roomString = @"请选择房间号";
    self.houseNumberString = [sender.userInfo objectForKey:@"houseName"];
    self.textColor = [UIColor blackColor];
    [self.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]]];
    [self.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]]];
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    self.verifyButton.enabled = (self.communityString.length && self.houseNumberString.length && self.roomString.length && self.squareMetersString.length );
}

- (void)setupFooterView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];
    self.verifyButton = [UIButton phc_actionButtonWithTitle:@"获取估值"];
    self.verifyButton.enabled = NO;
    [footerView addSubview:self.verifyButton];
    self.tableView.tableFooterView = footerView;

    [self.verifyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(footerView);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.9f);
        make.height.equalTo(@40);
    }];
    typeof(self) __weak weakSelf = self;
    
    [self.verifyButton bk_addEventHandler:^(UIButton *sender) {
        [weakSelf.view endEditing:YES];
        weakSelf.hud = [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];

        NSString *cityString = [weakSelf traverseAttributeDataArray:weakSelf.cityListArray name:weakSelf.cityString nameKey:kCityListKey IDKey:kCityListID];
        NSString *construction = [weakSelf traverseAttributeDataArray:weakSelf.communityListArray name:weakSelf.communityString nameKey:kCommunityListKey IDKey:kCommunityListID];
        NSString *building = [weakSelf traverseAttributeDataArray:weakSelf.houseNumberListArray name:weakSelf.houseNumberString nameKey:kHouseNumberListKey IDKey:kHouseNumberListID];
        NSString *room = [weakSelf traverseAttributeDataArray:weakSelf.roomListArray name:weakSelf.roomString nameKey:kRoomListKey IDKey:kRoomListID];
        NSString *address = [NSString stringWithFormat:@"%@%@%@%@",weakSelf.cityString,weakSelf.communityString,weakSelf.houseNumberString,weakSelf.roomString];
        [[weakSelf.objectManager taskToPriceCity:cityString construction:construction building:building room:room size:self.squareMetersString] continueWithBlock:^id(BFTask *task) {
            [weakSelf.hud hideAnimated:YES];
            if (task.error) {
                [weakSelf taskToShowError:task.error];
            }else {
                PHValuationResultTableViewController *valuationResultTVC = [PHValuationResultTableViewController new];
                valuationResultTVC.addressString = address;
                valuationResultTVC.valuationModel = ((PHAPIValuationResponse *)task.result).AvagePrice;
                [weakSelf.navigationController pushViewController:valuationResultTVC animated:YES];
            }
            return task;
        }];
    } forControlEvents:UIControlEventTouchUpInside];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    typeof(self) __weak weakSelf = self;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (indexPath.row == 1) {
            [[self.objectManager taskToProvincesList] continueWithBlock:^id(BFTask *task) {
                [weakSelf.hud hideAnimated:YES];
                if (task.error) {
                    [self taskToShowError:task.error];
                }else {
                    self.provinceListArray = [NSArray arrayWithArray:task.result];
                    [weakSelf loadProvinceList];
                }
                return task;
            }];
    }else if (indexPath.row == 2) {
        if (self.provinceListArray) {
            NSString *cityString = [self traverseAttributeDataArray:self.provinceListArray name:self.provinceString nameKey:kProvinceListKey IDKey:kProvinceListID];
            [[self.objectManager taskToCityList:cityString] continueWithBlock:^id(BFTask *task) {
                [weakSelf.hud hideAnimated:YES];
                if (task.error) {
                    [self taskToShowError:task.error];
                }else {
                    self.cityListArray = [NSArray arrayWithArray:task.result];
                    [self loadCityList];
                }
                return task;
            }];
        }
    }else if (indexPath.row == 3){
        if (self.cityListArray ) {
            NSString *cityString = [self traverseAttributeDataArray:self.cityListArray name:self.cityString nameKey:kCityListKey IDKey:kCityListID];
//            [[self.objectManager taskToCommunityList:cityString communityName:@"hi"] continueWithBlock:^id(BFTask *task) {
//                [weakSelf.hud hideAnimated:YES];
//                if (task.error) {
//                    [self taskToShowError:task.error];
//                }else {
//                    self.communityListArray = [NSArray arrayWithArray:task.result];
//                    [self loadCommunityList];
//                }
//                return task;
//            }];
            
            [weakSelf.hud hideAnimated:YES];

            PHSearchViewController *searchVC = [PHSearchViewController new];
            searchVC.cityString = cityString;
            searchVC.searchType = @"community";
            [self.navigationController pushViewController:searchVC animated:YES];

        }
    }else if (indexPath.row == 4){
        if (self.communityListArray) {
            NSString *cityString = [self traverseAttributeDataArray:self.cityListArray name:self.cityString nameKey:kCityListKey IDKey:kCityListID];
            NSString *construction = [self traverseAttributeDataArray:self.communityListArray name:self.communityString nameKey:kCommunityListKey IDKey:kCommunityListID];
            [[self.objectManager taskToHouseList:cityString construction:construction] continueWithBlock:^id(BFTask *task) {
                [weakSelf.hud hideAnimated:YES];
                if (task.error) {
                    [self taskToShowError:task.error];
                }else {
                    self.houseNumberListArray = [NSArray arrayWithArray:task.result];
                    //[self loadHouseList];
                    PHSearchViewController *searchVC = [PHSearchViewController new];
                    searchVC.searchType = @"house";
                    searchVC.houseNumberListArray = [self parsingDataListArray:self.houseNumberListArray valueForKey:kHouseNumberListKey];
                    [self.navigationController pushViewController:searchVC animated:YES];

                }
                return task;
            }];
        }
    }else if (indexPath.row == 5){
        if (self.houseNumberListArray) {
            NSString *cityString = [self traverseAttributeDataArray:self.cityListArray name:self.cityString nameKey:kCityListKey IDKey:kCityListID];
            NSString *building = [self traverseAttributeDataArray:self.houseNumberListArray name:self.houseNumberString nameKey:kHouseNumberListKey IDKey:kHouseNumberListID];
            [[self.objectManager taskToRoomList:cityString building:building] continueWithBlock:^id(BFTask *task) {
                [weakSelf.hud hideAnimated:YES];
                if (task.error) {
                    [self taskToShowError:task.error];
                }else {
                    self.roomListArray = [NSArray arrayWithArray:task.result];
                    [self loadRoomList];
                }
                return task;
            }];
        }
    }else {
        [weakSelf.hud hideAnimated:YES];
    }
}

- (void)loadProvinceList {
    typeof(self) __weak weakSelf = self;
    [weakSelf.hud hideAnimated:YES];
    [[self taskToShowPickerDataArray:[self parsingDataListArray:self.provinceListArray valueForKey:kProvinceListKey]] continueWithBlock:^id(BFTask *task) {
        weakSelf.provinceString = task.result;
        weakSelf.textColor = [UIColor blackColor];
        [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]]];
        return task;
    }];
}

- (void)loadCityList {
    [self.hud hideAnimated:YES];
    if (self.cityListArray) {
        typeof(self) __weak weakSelf = self;
        [[self taskToShowPickerDataArray:[self parsingDataListArray:self.cityListArray valueForKey:kCityListKey]] continueWithBlock:^id(BFTask *task) {
            weakSelf.cityString = task.result;
            weakSelf.textColor = [UIColor blackColor];
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]]];
            return task;
        }];
    }
}

- (void)loadCommunityList {
    [self.hud hideAnimated:YES];
    if (self.communityListArray) {
        typeof(self) __weak weakSelf = self;
        [[self taskToShowPickerDataArray:[self parsingDataListArray:self.communityListArray valueForKey:kCommunityListKey]] continueWithBlock:^id(BFTask *task) {
            weakSelf.communityString = task.result;
            weakSelf.textColor = [UIColor blackColor];
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]]];
            return task;
        }];
    }
}

- (void)loadHouseList {
    [self.hud hideAnimated:YES];
    if (self.houseNumberListArray) {
        typeof(self) __weak weakSelf = self;
        [[self taskToShowPickerDataArray:[self parsingDataListArray:self.houseNumberListArray valueForKey:kHouseNumberListKey]] continueWithBlock:^id(BFTask *task) {
            weakSelf.houseNumberString = task.result;
            weakSelf.textColor = [UIColor blackColor];
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]]];
            return task;
        }];
    }
}

- (void)loadRoomList {
    [self.hud hideAnimated:YES];
    if (self.roomListArray) {
        typeof(self) __weak weakSelf = self;
        [[self taskToShowPickerDataArray:[self parsingDataListArray:self.roomListArray valueForKey:kRoomListKey]] continueWithBlock:^id(BFTask *task) {
            weakSelf.roomString = task.result;
            weakSelf.textColor = [UIColor blackColor];
            [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]]];
            return task;
        }];
    }
}

- (NSString *)traverseAttributeDataArray:(NSArray *)array name:(NSString *)name nameKey:(NSString *)nameKey IDKey:(NSString *)IDKey {
    for (int i = 0; i < array.count; i++) {
        if ([[[array objectAtIndex:i]valueForKey:nameKey] isEqualToString:name]) {
            return [[array objectAtIndex:i]valueForKey:IDKey];
        }
    }
    return nil;
}

- (NSArray *)parsingDataListArray:(NSArray *)array valueForKey:(NSString *)key{
    
    NSMutableArray *mutableArray = [NSMutableArray new];
    for (int i = 0; i < array.count; i++) {
        [mutableArray addObject:[[array objectAtIndex:i] valueForKey:key]];
    }
    return mutableArray;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObserver:self forKeyPath:@"provinceString"];
    [self removeObserver:self forKeyPath:@"cityString"];
    [self removeObserver:self forKeyPath:@"communityString"];
    [self removeObserver:self forKeyPath:@"houseNumberString"];
    [self removeObserver:self forKeyPath:@"roomString"];
    [self removeObserver:self forKeyPath:@"squareMetersString"];

}
@end
