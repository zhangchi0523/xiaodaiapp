//
//  PHApplications.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHApplications.h"
@interface PHApplications()
@property (nonatomic, readwrite, copy) NSString *name;
@property (nonatomic, readwrite, copy) NSString *amount;
@property (nonatomic, readwrite, copy) NSString *phone;
@property (nonatomic, readwrite, copy) NSString *identificationNumber;
@property (nonatomic, readwrite, copy) NSString *identificationFront;
@property (nonatomic, readwrite, copy) NSString *identificationBack;
@property (nonatomic, readwrite, copy) NSString *hukouFront;
@property (nonatomic, readwrite, copy) NSString *hukouBack;
@property (nonatomic, readwrite, copy) NSString *deed;
@property (nonatomic, readwrite, copy) NSString *type;
@property (nonatomic, readwrite, copy) NSString *valuationId;
@property (nonatomic, readwrite, copy) NSString *address;
@property (nonatomic, readwrite, copy) NSMutableArray *imageArray;



@end
@implementation PHApplications

- (instancetype)initWithName:(NSString *)name withAmount:(NSString *)amount withPhone:(NSString *)phone withIdentificationNumber:(NSString *)idNumber withidentificationFront:(NSString *)idFront withidentificationBack:(NSString *)idBack withHukouFront:(NSString *)hukouFront withHukouBack:(NSString *)hukouBack withDeed:(NSString *)deed withType:(NSString *)type withValuationId:(NSString *)valuationId withAddress:(NSString *)address withImageArray:(NSMutableArray *)imageArray{
    if (self = [super init]) {
        _amount =[amount copy];
        _name = [name copy];
        _phone = [phone copy];
        _identificationNumber =[idNumber copy];
        _identificationFront = [idFront copy];
        _identificationBack = [idBack copy];
        _hukouFront = [hukouFront copy];
        _hukouBack = [hukouBack copy];
        _deed = [deed copy];
        _type = [type copy];
        _valuationId = [valuationId copy];
        _address = [address copy];
        _imageArray = [imageArray copy];
    
    
    }
    
    return self;
}
-(void)updateImageArray:(NSArray *)imageArray{
    self.imageArray = [imageArray copy];

}

@end
