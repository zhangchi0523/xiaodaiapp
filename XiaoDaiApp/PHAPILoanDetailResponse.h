//
//  PHAPILoanDetailResponse.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/5.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIBaseResponse.h"

@interface PHAPILoanDetailResponse : PHAPIBaseResponse
@property (nonatomic, readonly, copy) NSNumber *amount;
@property (nonatomic, readonly, copy) NSNumber *duration;
@property (nonatomic, readonly, copy) NSNumber *valuation;
@property (nonatomic, readonly, copy) NSString *applicantName;
@property (nonatomic, readonly, copy) NSString *status;

@property (nonatomic, readonly, copy) NSString *applicantPhone;
@property (nonatomic, readonly, copy) NSString *applicantIdentificationNumber;
@property (nonatomic, readonly, copy) NSString *agentName;
@property (nonatomic, readonly, copy) NSString *agentPhone;
@property (nonatomic, readonly, copy) NSString *agentIdentificationNumber;
@property (nonatomic, readonly, copy) NSString *address;
@property (nonatomic, readonly, copy) NSString *type;

@property (nonatomic, readonly, copy) NSNumber *buildArea;
@property (nonatomic, readonly, copy) NSNumber *unitPrice;
@property (nonatomic, readonly, copy) NSNumber *avagePrice;
@property (nonatomic, readonly, copy) NSNumber *maxPrice;
@property (nonatomic, readonly, copy) NSNumber *minPrice;
@property (nonatomic, readonly, copy) NSNumber *appId;

@property (nonatomic, readonly, copy) NSString *identificationFront;
@property (nonatomic, readonly, copy) NSString *identificationBack;
@property (nonatomic, readonly, copy) NSString *huKouFront;
@property (nonatomic, readonly, copy) NSString *hukouBack;
@property (nonatomic, readonly, copy) NSString *deed;
@property (nonatomic, strong, readonly) NSArray *mediaArray;

@property (nonatomic, readonly, copy) NSNumber *branchId;
@property (nonatomic, readonly, copy) NSString *branchName;

- (NSString *)phc_amountString;
- (NSString *)phc_valuationString;
- (NSString *)phc_unitPriceString;
- (NSString *)phc_minPriceString;
- (NSString *)phc_maxPriceString;
- (NSString *)phc_avagePriceString;





@end
