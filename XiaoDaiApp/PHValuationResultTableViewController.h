//
//  PHValuationResultTableViewController.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewController.h"

@class PHValuationModel;

@interface PHValuationResultTableViewController : PHTableViewController

@property (nonatomic, strong) PHValuationModel *valuationModel;
@property (nonatomic, strong) NSString *addressString;

@end
