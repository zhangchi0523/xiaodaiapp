//
//  PHValuationModel.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PHValuationModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *Note;
@property (nonatomic, readonly, copy) NSNumber *Amount;
@property (nonatomic, readonly, copy) NSNumber *AvagePrice;
@property (nonatomic, readonly, copy) NSNumber *BuildArea;
@property (nonatomic, readonly, copy) NSNumber *CaseCount;
@property (nonatomic, readonly, copy) NSNumber *MaxPrice;
@property (nonatomic, readonly, copy) NSNumber *MinPrice;
@property (nonatomic, readonly, copy) NSNumber *Status;
@property (nonatomic, readonly, copy) NSNumber *UnitPrice;
@property (nonatomic, readonly, copy) NSNumber *number_id;

@end
