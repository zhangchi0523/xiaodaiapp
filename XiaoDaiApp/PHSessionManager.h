//
//  PHSessionManager.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BFTask;
@class PHUserProfile;
extern NSString * const PHNotificationLogout;

@interface PHSessionManager : NSObject

@property (nonatomic, strong, readonly) PHUserProfile* currentUserProfile;

@property (nonatomic, copy) NSString *user_token;
@property (nonatomic, copy, readonly) NSString *username;
@property (nonatomic, copy, readonly) NSString *password;
@property (nonatomic, readonly, assign) BOOL isLoggedIn;

+ (instancetype)sharedInstance;
- (BFTask *)taskToPerformLogout;
@end
