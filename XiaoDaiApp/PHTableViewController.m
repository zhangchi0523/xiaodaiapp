//
//  PHTableViewController.m
//  yqrx
//
//  Created by 1234 on 15/11/25.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import "PHTableViewController.h"
#import <Bolts/Bolts.h>
#import <AFNetworking/AFNetworking.h>

@interface PHTableViewController ()

@end

@implementation PHTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (NSString *)analyticsName {
    return NSStringFromClass(self.class);
}
- (BFTask *)taskToShowError:(NSError *)error {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    NSData *responseData = (NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:[dic objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    return completionSource.task;
}
- (BFTask *)taskToShowPrompt:(NSString *)prompt {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:prompt preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [completionSource setResult:@YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    return completionSource.task;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
