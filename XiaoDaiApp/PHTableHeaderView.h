//
//  PHTableHeaderView.h
//  p2p
//
//  Created by Philip on 9/15/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHTableHeaderView : UIView

- (instancetype)initWithTitles:(NSArray<NSString *> *)titles withInsets:(UIEdgeInsets)edgeInsets;

@end
