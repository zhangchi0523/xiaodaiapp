//
//  PHFoundViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/7.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHFoundViewController.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHMenuTableViewCell.h"
#import <Masonry.h>
#import "UIColor+phc_addition.h"
#import "PHDoclistViewController.h"
#import "PHStepsViewController.h"
#import "PHCalculatorViewController.h"
@interface PHFoundViewController ()
@property (nonatomic, strong) PHTableViewDataSource *dataSource;

@end

@implementation PHFoundViewController
-(instancetype)init{
    if (self = [super init]) {
        self.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"" image:[UIImage imageNamed:@"found_icon"] selectedImage:nil];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(5.0f, 0.0f, -5.0f, 0.0f);
        self.navigationItem.title = @"发现";

    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor phc_lightGray];
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"贷款计算器" details:nil withLeftImage:[UIImage imageNamed:@"found_ calculator"]];
                       }collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"房屋抵押流程" details:nil withLeftImage:[UIImage imageNamed:@"found_ pledge"]];
                       }collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"预约资料清单" details:nil withLeftImage:[UIImage imageNamed:@"found_ appointment"]];
                       }collationStringSelector:nil],

                       ];
    [self.dataSource addItems:items];
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f,80.0f)];
    self.tableView.tableFooterView = footerView;


}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 1:{
            [self.navigationController pushViewController:[PHCalculatorViewController new] animated:YES];
        }
            break;
        case 3:{
            [self.navigationController pushViewController:[PHStepsViewController new] animated:NO];
        }
            break;
        case 4:{
            [self.navigationController pushViewController:[PHDoclistViewController new] animated:NO];
        }
            break;

        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
