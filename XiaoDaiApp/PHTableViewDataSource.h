//
//  PHTableViewDataSource.h
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PHTableViewModel;
@interface PHTableViewDataSource : NSObject

- (instancetype)initWithTableView:(UITableView *)tableView;

- (instancetype)initWithTableView:(UITableView *)tableView showIndexTitle:(BOOL)shouldShowIndexTitle;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

- (void)addItems:(NSArray<PHTableViewModel *> *)items;

- (void)removeAllItems;

- (void)reloadData;

- (void)reloadDataAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths;

- (void)clearRenderQueue;
@end
