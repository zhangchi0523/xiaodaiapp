//
//  PHTableViewController.h
//  yqrx
//
//  Created by 1234 on 15/11/25.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BFTask;

@interface PHTableViewController : UITableViewController

- (NSString *)analyticsName;
- (BFTask *)taskToShowError:(NSError *)error;
- (BFTask *)taskToShowPrompt:(NSString *)prompt;
@end
