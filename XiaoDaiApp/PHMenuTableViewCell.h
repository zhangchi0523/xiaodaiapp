//
//  PHKeyValueTableViewCell.h
//  p2p
//
//  Created by Philip on 10/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTableViewCell.h"

@interface PHMenuTableViewCell : PHTableViewCell

@property (nonatomic, readonly, strong) UILabel *leftLabel;
@property (nonatomic, readonly, strong) UILabel *rightLabel;
@property (nonatomic, readonly,strong)UIImageView *leftImageView;
- (void)configureWithTitle:(NSString *)title details:(NSString *)details withLeftImage:(UIImage *)image;

- (void)configureWithTitle:(NSString *)title details:(NSString *)details withDetailsFont:(NSInteger)detailsFont withDetailsColor:(UIColor *)detailsColor  withLeftImage:(UIImage *)image;

- (void)showLine;


@end
