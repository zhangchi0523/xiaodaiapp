//
//  PHTableViewModel.h
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PHTableViewModel : NSObject

@property (nonatomic, readonly, strong) id item;
@property (nonatomic, readonly, copy) Class cellClass;
@property (nonatomic, readonly, assign) SEL collationStringSelector;
@property (nonatomic, readonly, copy) void (^configurationBlock)(UITableViewCell *cell, __weak id item);

- (instancetype)initWithItem:(id)item cellClass:(Class)cellClass configurationBlock:(void(^)(UITableViewCell *cell, __weak id item))configurationBlock collationStringSelector:(SEL)collationStringSelector;

- (void)updateItem:(id)newItem;

@end
