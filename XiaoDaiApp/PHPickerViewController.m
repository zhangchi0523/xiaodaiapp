//
//  PHPickerViewController.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHPickerViewController.h"
#import <Masonry/Masonry.h>
#import "UIColor+phc_addition.h"
#import "UIButton+phc_addition.h"
#import "UILabel+phc_addition.h"
#import <Bolts/Bolts.h>
#import "UIView+BlocksKit.h"

static const float mainViewHeight = 240;
static const float topDistance = 40;

@interface PHPickerViewController () <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, copy) void(^dismissHandler)(PHPickerViewController *);
@property (nonatomic, strong)UIView *mainView;
@property (nonatomic, strong)NSArray *dataArray;


@end

@implementation PHPickerViewController

- (instancetype)initWithContentArray:(NSArray *)array DismissHandler:(void(^)(PHPickerViewController *controller))dismissHandler{
    if (self = [super init]) {
        _dismissHandler = dismissHandler;
        _dataArray = array;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    typeof(self) __weak weakSelf = self;
    self.tabBarController.tabBar.hidden=YES;

    self.mainView = [[UIView alloc]init];
    self.mainView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mainView];
    
    if (self.dataArray.count > 0) {
        self.dataString = [self.dataArray objectAtIndex:0];
    }
    
    UIPickerView *pickerView = [UIPickerView new];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.backgroundColor = [UIColor phc_gray];
    [self.mainView addSubview:pickerView];
    
    UILabel *completeLabel = [UILabel phc_labelWithFontSize:15.0f textColor:[UIColor phc_blue] backgroundColor:nil numberOfLines:1];
    completeLabel.text = @"完成";
    completeLabel.userInteractionEnabled = YES;
    
    [self.mainView addSubview:completeLabel];
    
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(mainViewHeight));
        make.bottom.equalTo(weakSelf.view.mas_bottom);
        make.width.equalTo(weakSelf.view.mas_width);
    }];
    
    [pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(topDistance));
        make.height.equalTo(weakSelf.mainView.mas_height).with.offset(-40.0f);
        make.bottom.equalTo(weakSelf.mainView.mas_bottom);
        make.width.equalTo(weakSelf.mainView.mas_width);
    }];

    [completeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@5.0f);
        make.right.equalTo(@(-10.f));
        make.width.equalTo(@40.0f);
        make.height.equalTo(@30.0f);
    }];
    
    [completeLabel bk_whenTapped:^{
        if (self.dismissHandler) {
            self.dismissHandler(self);
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.dataArray objectAtIndex:row];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.dataString = [self.dataArray objectAtIndex:row];
}

@end
