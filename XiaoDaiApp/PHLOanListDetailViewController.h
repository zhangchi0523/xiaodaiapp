//
//  PHLOanListDetailViewController.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewController.h"
@class PHLoan;
@interface PHLOanListDetailViewController : PHTableViewController
- (instancetype)initWithLoan:(id)loanDetail;
@end
