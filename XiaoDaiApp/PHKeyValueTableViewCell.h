//
//  PHKeyValueTableViewCell.h
//  p2p
//
//  Created by Philip on 10/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTableViewCell.h"

@interface PHKeyValueTableViewCell : PHTableViewCell

@property (nonatomic, readonly, strong) UILabel *leftLabel;
@property (nonatomic, readonly, strong) UILabel *rightLabel;

- (void)configureWithTitle:(NSString *)title details:(NSString *)details;
- (void)configureWithTitle:(NSString *)title details:(NSString *)details textColor:(UIColor *)color;
- (void)configureWithTitle:(NSString *)title details:(NSString *)details rightAlignmentMode:(BOOL)rightAlignmentMode textColor:(UIColor *)color;
- (void)configureWithCellAccessoryType :(UITableViewCellAccessoryType)cellAccessoryType ;
-(void)setTitleTextColor:(UIColor *)color;
@end
