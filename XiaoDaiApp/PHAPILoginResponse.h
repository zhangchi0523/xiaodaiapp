//
//  PHAPILoginResponse.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIBaseResponse.h"
@class PHUserProfile;

@interface PHAPILoginResponse : PHAPIBaseResponse

@property (nonatomic, strong, readonly) PHUserProfile *user;

@end
