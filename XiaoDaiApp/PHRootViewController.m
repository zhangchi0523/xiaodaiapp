//
//  PHRootViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/14.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHRootViewController.h"
#import "HomePageViewController.h"
#import "PHLoginViewController.h"
#import <Bolts.h>
#import "UIColor+phc_addition.h"
#import "PHSessionManager.h"
#import "PHLoanListViewController.h"
#import "PHFoundViewController.h"
#import "PHMyViewController.h"
#import "PHPersonalCenterViewController.h"
@interface PHRootViewController ()

@property (nonatomic, strong) PHSessionManager *sessionManager;
@property (nonatomic, strong) id logoutObserver;

@end

@implementation PHRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    typeof(self) __weak weakSelf = self;
    self.sessionManager = [PHSessionManager sharedInstance];

    self.tabBar.alpha = 0.0f;
    self.tabBarController.tabBar.delegate = self;
    self.view.backgroundColor = [UIColor whiteColor];
    self.logoutObserver = [[NSNotificationCenter defaultCenter] addObserverForName:PHNotificationLogout object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [weakSelf presentLoginFlow:self.sessionManager.isLoggedIn];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (!self.sessionManager.isLoggedIn) {
        [self presentLoginFlow:self.sessionManager.isLoggedIn];
    }else {
        [self loadMainViewController];
    }
}

- (void)loadMainViewController {
    if (self.viewControllers.count) return;
    self.tabBar.alpha = 1.0f;

    self.viewControllers = @[
                             [[UINavigationController alloc] initWithRootViewController:[HomePageViewController new]],
                             [[UINavigationController alloc] initWithRootViewController:[PHFoundViewController new]],
                             [[UINavigationController alloc] initWithRootViewController:[PHPersonalCenterViewController new]],];
}

- (BFTask *)presentLoginFlow:(BOOL)animated {
    if (animated) return [BFTask taskWithResult:@YES];
    
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    typeof(self) __weak weakSelf = self;
    
    PHLoginViewController *loginViewController = [[PHLoginViewController alloc]init];
    [loginViewController setSuccessHandler:^(PHLoginViewController *loginViewController) {
        [weakSelf loadMainViewController];
        [loginViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    self.viewControllers = nil;

    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:loginViewController] animated:animated completion:^{
        [completionSource setResult:@YES];
    }];
    return completionSource.task;
}

@end
