//
//  PHLoginViewController.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHViewController.h"

@interface PHLoginViewController : PHViewController

@property (nonatomic, copy) void (^successHandler)(PHLoginViewController *loginViewController);

@end
