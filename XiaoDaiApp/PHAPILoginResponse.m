//
//  PHAPILoginResponse.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPILoginResponse.h"
#import "PHUserProfile.h"

@interface PHAPILoginResponse()

@property (nonatomic, strong, readwrite) PHUserProfile *user;

@end

@implementation PHAPILoginResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    [mapping setObject:@"data.user" forKey:@"user"];
    return mapping;
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:PHUserProfile.class];
}

@end
