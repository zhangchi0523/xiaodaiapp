//
//  PHAPIGetLoanListResponse.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHAPIGetLoanListResponse.h"
@interface PHAPIGetLoanListResponse ()

@property (nonatomic, strong, readwrite) NSArray *loanList;
@property (nonatomic, strong, readwrite) NSNumber *total;
@property (nonatomic, strong, readwrite) NSNumber *page;

@end
@implementation PHAPIGetLoanListResponse
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey]mutableCopy];
    [mapping setObject:@"data.items" forKey:@"loanList"];
    [mapping setObject:@"data.total" forKey:@"total"];
    [mapping setObject:@"data.page" forKey:@"page"];

    return mapping;
}

+ (NSValueTransformer *)loanListJSONTransformer{
    return [MTLJSONAdapter arrayTransformerWithModelClass:PHLoan.class];
}
@end
