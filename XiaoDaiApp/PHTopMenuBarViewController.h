//
//  PHTopMenuBarViewController.h
//  p2p
//
//  Created by Philip on 9/12/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
    置顶滑动菜单
 */
@interface PHTopMenuBarViewController : UIViewController

/**
    @brief 创建及返回一个实例
 
    @param selectedMenuIndex 指定菜单的初始选中项，默认为第一个，index ＝ 0
    @return 实例
 */
- (instancetype)initWithSelectedMenuIndex:(NSUInteger)selectedMenuIndex;

/**
    @brief 返回一个 UIViewController 的排列，用来生成置顶的菜单选项

    @return Controller 队列
 */
- (NSArray<UIViewController *> *)menuViewControllers;

- (void)loadControllerAtIndex:(NSUInteger)index;
@end
