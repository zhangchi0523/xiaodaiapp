//
//  PHRegisterViewController.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHRegisterViewController.h"
#import "UIColor+phc_addition.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHTextFieldTableViewCell.h"
#import "PHKeyValueTableViewCell.h"
#import "UIControl+BlocksKit.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import <AFNetworking/AFNetworking.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import "PHAPIGetBranchResponse.h"
@interface PHRegisterViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) NSString *nameString;
@property (nonatomic, strong) NSString *passwordString;
@property (nonatomic, strong) NSString *phoneString;
@property (nonatomic, strong) NSString *IDCardstring;
@property (nonatomic, strong) NSString *channelString;
@property (nonatomic, strong) PHObjectManager *objectManager;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic,strong) UIColor *textColor;
@property (nonatomic, strong) NSString *branchId;

@end

@implementation PHRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textColor = [UIColor phc_gray];
    self.channelString= @"请选择渠道号";
    self.title = NSLocalizedString(@"RegisterTitle", nil);
    self.view.backgroundColor = [UIColor phc_lightGray];
    self.objectManager = [PHObjectManager sharedInstance];
    self.tableView.delegate = self;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];

    self.registerButton = [UIButton phc_actionButtonWithTitle:NSLocalizedString(@"RegisterTitle", nil)];
    self.registerButton.enabled = NO;
    [footerView addSubview:self.registerButton];
    [self.registerButton addTarget:self action:@selector(registerButtonButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = footerView;

    [self.registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(footerView);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.7f);
    }];
    typeof(self) __weak weakSelf = self;

    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:NSLocalizedString(@"accountTitle", nil) placeholder:NSLocalizedString(@"promptInputAccount", nil) text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.phoneString = textField.text;
                           } forControlEvents:UIControlEventEditingChanged];
                           c.textField.delegate = weakSelf;
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:NSLocalizedString(@"passWordTitle", nil) placeholder:NSLocalizedString(@"promptInputPassWord", nil) text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.passwordString = textField.text;
                           } forControlEvents:UIControlEventEditingChanged];
                           c.textField.delegate = weakSelf;

                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHKeyValueTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHKeyValueTableViewCell *c = (PHKeyValueTableViewCell *)cell;
                           [c configureWithTitle:@"渠道号" details:weakSelf.channelString textColor:weakSelf.textColor];
                           [c configureWithCellAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           
                           [c configureWithTitle:NSLocalizedString(@"idCardTitle", nil) placeholder:NSLocalizedString(@"promptInputidCard", nil) text:nil];
                           [c configureWithKeyBoardType:UIKeyboardTypeNumberPad withIdNumTextField:NO];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.IDCardstring = textField.text;
                           } forControlEvents:UIControlEventEditingChanged];
                           c.textField.delegate = weakSelf;

                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"姓名" placeholder:@"请输入姓名" text:nil];
                           [c configureWithKeyBoardType:UIKeyboardTypeNumberPad withIdNumTextField:NO];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.nameString = textField.text;
                           } forControlEvents:UIControlEventEditingChanged];
                           c.textField.delegate = weakSelf;

                       } collationStringSelector:nil]
                       ];
    
    [self.dataSource addItems:items];
    
    [self addObserver:self forKeyPath:@"nameString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"passwordString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"channelString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"IDCardstring" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"phoneString" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];

}

- (void)registerButtonButtonTapped:(UIButton *)button {
    typeof(self) __weak weakSelf = self;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[self.objectManager taskToRegisterAccountWithName:self.nameString PhoneNumber:self.phoneString password:self.passwordString identificationNumber:self.IDCardstring withBranchId:self.branchId] continueWithBlock:^id(BFTask *task) {
        [weakSelf.hud hideAnimated:YES];
        if (task.error) {
            [self taskToShowError:task.error];
        }else {
            [[self taskToShowPrompt:@"注册成功"] continueWithBlock:^id(BFTask *task) {
                [self.navigationController popViewControllerAnimated:YES];
                return task;
            }];
        }
        return task;
    }];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==3) {
        typeof(self) __weak weakSelf = self;
        [[self.objectManager taskToGetBranch]continueWithBlock:^id(BFTask *task){
            if (task.error) {
                [weakSelf taskToShowError:task.error];
            }else{
                PHAPIGetBranchResponse *response = (PHAPIGetBranchResponse *)task.result;
                NSArray *arrayBranch = [response.data bk_map:^id(NSDictionary *dic){
                    return [[dic objectForKey:@"name"] stringByRemovingPercentEncoding];
                }];
                [[weakSelf taskToShowPickerDataArray:arrayBranch]continueWithBlock:^id(BFTask *task) {
                    weakSelf.textColor = [UIColor blackColor];
                    weakSelf.channelString = task.result;
                    
                    [weakSelf.dataSource reloadDataAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]]];
                    for (NSDictionary *dic in response.data) {
                        if ([[dic objectForKey:@"name"] isEqualToString:weakSelf.channelString]) {
                            weakSelf.branchId = [dic objectForKey:@"id"];
                        }
                    }
                    return task;
                }];
            }
            return task;
        }];
    }
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    self.registerButton.enabled = (self.phoneString.length && self.passwordString.length && self.channelString.length && self.IDCardstring.length&&self.nameString);
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"nameString"];
    [self removeObserver:self forKeyPath:@"passwordString"];
    [self removeObserver:self forKeyPath:@"channelString"];
    [self removeObserver:self forKeyPath:@"IDCardstring"];
    [self removeObserver:self forKeyPath:@"phoneString"];

}

@end
