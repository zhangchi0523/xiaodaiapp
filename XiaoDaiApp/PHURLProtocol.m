//
//  PHURLProtocol.m
//  yqrx
//
//  Created by Philip on 11/17/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHURLProtocol.h"

static NSString * const kHeaderFlagField = @"PHCustomFlagSet";
static NSString * const kHeaderUserAgentField = @"PHCustomFlagSet";

@interface PHURLProtocol()
@property (nonatomic, strong) NSURLConnection *connection;
@end

@implementation PHURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    return (![[NSURLProtocol propertyForKey:kHeaderFlagField inRequest:request] boolValue]);
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}

- (void)startLoading {
    NSMutableURLRequest *mutableRequest = [self.request mutableCopy];
    NSString *userAgent = [mutableRequest valueForHTTPHeaderField:kHeaderUserAgentField];
    
    if (!userAgent.length) userAgent = @"Mozilla/5.0";
    userAgent = [userAgent stringByAppendingString:@" yqrx-app"];
    
    [mutableRequest setValue:userAgent forHTTPHeaderField:kHeaderUserAgentField];
    [NSURLProtocol setProperty:@YES forKey:kHeaderFlagField inRequest:mutableRequest];
    self.connection = [NSURLConnection connectionWithRequest:mutableRequest delegate:self];
}

- (void)stopLoading {
    [self.connection cancel];
}

#pragma mark - NSURLConnectionDelegate

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
    if (redirectResponse) {
        [self.client URLProtocol:self wasRedirectedToRequest:request redirectResponse:redirectResponse];
        return request;
    }
    
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.client URLProtocol:self didLoadData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.client URLProtocol:self didFailWithError:error];
    self.connection = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.client URLProtocolDidFinishLoading:self];
    self.connection = nil;
}

@end
