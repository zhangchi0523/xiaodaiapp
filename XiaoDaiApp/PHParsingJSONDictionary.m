//
//  PHParsingJSONDictionary.m
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/19.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHParsingJSONDictionary.h"

@interface PHParsingJSONDictionary ()

@property (nonatomic, readwrite, copy) NSString *ProvinceId;
@property (nonatomic, readwrite, copy) NSString *ProvinceName;

@end

@implementation PHParsingJSONDictionary

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"ProvinceId":@"ProvinceId",
             @"ProvinceName":@"ProvinceName",
             };
}

+ (NSValueTransformer *)verifiedJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)isCreditDownJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
