//
//  UIImage+phc_addition.m
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIImage+phc_addition.h"

@implementation UIImage (phc_addition)

+ (UIImage *)phc_imageWithColor:(UIColor *)color {
    return [self phc_imageWithColor:color height:1.0f];
}

+ (UIImage *)phc_imageWithColor:(UIColor *)color height:(CGFloat)height {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)phc_imageToReszieWithsize:(CGSize)size compressionQuality:(CGFloat)compressionQuality;
{
    NSData *editedData = UIImageJPEGRepresentation(self, compressionQuality);
    UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
    [[UIImage imageWithData:editedData] drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return reSizeImage;
}


@end
