//
//  PHApplications.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/20.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PHApplications : NSObject
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *amount;
@property (nonatomic, readonly, copy) NSString *phone;
@property (nonatomic, readonly, copy) NSString *identificationNumber;
@property (nonatomic, readonly, copy) NSString *identificationFront;
@property (nonatomic, readonly, copy) NSString *identificationBack;
@property (nonatomic, readonly, copy) NSString *hukouFront;
@property (nonatomic, readonly, copy) NSString *hukouBack;
@property (nonatomic, readonly, copy) NSString *deed;
@property (nonatomic, readonly, copy) NSString *type;
@property (nonatomic, readonly, copy) NSString *valuationId;
@property (nonatomic, readonly, copy) NSString *address;
@property (nonatomic, readonly, copy) NSMutableArray *imageArray;
- (instancetype)initWithName:(NSString *)name withAmount:(NSString *)amount withPhone:(NSString *)phone withIdentificationNumber:(NSString *)idNumber withidentificationFront:(NSString *)idFront withidentificationBack:(NSString *)idBack withHukouFront:(NSString *)hukouFront withHukouBack:(NSString *)hukouBack withDeed:(NSString *)deed withType:(NSString *)type withValuationId:(NSString *)valuationId withAddress:(NSString *)address withImageArray:(NSMutableArray *)imageArray;
-(void)updateImageArray:(NSArray *)imageArray;

@end
