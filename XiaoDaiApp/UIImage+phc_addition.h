//
//  UIImage+phc_addition.h
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (phc_addition)

+ (UIImage *)phc_imageWithColor:(UIColor *)color;

+ (UIImage *)phc_imageWithColor:(UIColor *)color height:(CGFloat)height;

- (UIImage *)phc_imageToReszieWithsize:(CGSize)size compressionQuality:(CGFloat)compressionQuality;

@end
