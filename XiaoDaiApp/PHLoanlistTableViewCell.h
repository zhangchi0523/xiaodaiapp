//
//  PHLoanlistTableViewCell.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewCell.h"
#import "PHLoan.h"
@interface PHLoanlistTableViewCell : PHTableViewCell
-(void)configureWithLoan:(PHLoan *)loan;

@end
