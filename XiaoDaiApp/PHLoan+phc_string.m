//
//  PHLoan+String.m
//  p2p
//
//  Created by Philip on 9/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHLoan+phc_string.h"

@implementation PHLoan (phc_string)

- (NSDateFormatter *)dateFormatter {
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"yyyy-MM-dd";
    });
    
    return formatter;
}

- (NSNumberFormatter *)currencyFormatter {
    static NSNumberFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.groupingSeparator = @",";
        formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
    });
    
    return formatter;
}

- (NSNumberFormatter *)rateFormatter {
    static NSNumberFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = kCFNumberFormatterPercentStyle;
    });
    
    return formatter;
}
- (NSString *)phc_amountString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.amount doubleValue]]]];
}
- (NSString *)phc_valuationString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:[self.valuation doubleValue]]]];
}
-(NSString *)stringByTrimming:(NSString *)str{
    NSString *subStr = [str substringWithRange:NSMakeRange(0, 1)];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:subStr];
    return [str stringByTrimmingCharactersInSet:set];
}
@end
