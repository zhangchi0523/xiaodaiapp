//
//  PHLoanListViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHLoanListViewController.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHTableHeaderView.h"
#import "UIColor+phc_addition.h"
#import "UIViewController+phc_addition.h"
#import <Bolts/Bolts.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import "PHObjectManager.h"
#import "PHLoan.h"
#import "PHAPIGetLoanListResponse.h"
#import "PHLoanlistTableViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHLOanListDetailViewController.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
@interface PHLoanListViewController ()<UITableViewDelegate>
@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) PHTableHeaderView *headerView;
@property (nonatomic, strong) NSMutableArray *Loans;
@property (nonatomic,assign)NSInteger pageNum;
@property (nonatomic, strong) UIButton *nextPageButton;
@property (nonatomic, strong) UIButton *beforePageButton;
@property (nonatomic,assign)NSInteger total;

@end

@implementation PHLoanListViewController

- (instancetype)init {
    if (self = [super init]) {
        _Loans = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageNum = 1;
    self.navigationItem.title = @"申请管理";
    self.tableView.bounces = NO;
    self.headerView = [[PHTableHeaderView alloc] initWithTitles:@[ @"金额(元)", @"期限(天)", @"估值(元)", @"申请人", @"状态"] withInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 30.0f)];
    self.headerView.frame = CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 50.0f);
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    self.tableView.delegate = self;
    [self setUpFootView];
    [self addObserver:self forKeyPath:@"pageNum" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [self refreshList];
    if(self.beforePageButton&&self.nextPageButton){
        self.beforePageButton.hidden = NO;
        self.nextPageButton.hidden = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if(self.beforePageButton&&self.nextPageButton){
        self.beforePageButton.hidden = YES;
        self.nextPageButton.hidden = YES;
    }
}

-(void)setUpFootView{
    typeof(self) __weak weakSelf = self;
    UIView *footerView =[[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];
    self.tableView.tableFooterView = footerView;
    self.tableView.clipsToBounds = YES;
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    NSInteger screenWidth = screenFrame.size.width;
    NSInteger screenHeight = screenFrame.size.height;
    self.beforePageButton = [UIButton phc_actionButtonWithTitle:@"上一页"];
    self.beforePageButton.alpha = 0;
    self.beforePageButton.frame =CGRectMake(screenWidth*0.1f, screenHeight*0.8f, screenWidth*0.3, 40.0f);
    self.nextPageButton = [UIButton phc_actionButtonWithTitle:@"下一页"];
    self.nextPageButton.alpha = 0;
    self.nextPageButton.frame =CGRectMake(screenWidth*0.6f, screenHeight*0.8f, screenWidth*0.3, 40.0f);
    [[UIApplication sharedApplication].keyWindow addSubview:self.beforePageButton];
    [[UIApplication sharedApplication].keyWindow addSubview:self.nextPageButton];
    [self.beforePageButton bk_whenTapped:^{
        weakSelf.pageNum--;
        [self refreshList];
    }];
    [self.nextPageButton bk_whenTapped:^{
        weakSelf.pageNum++;
        [self refreshList];
    }];
}

-(void)refreshList{
    typeof(self) __weak weakSelf = self;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[PHObjectManager sharedInstance] taskToGetLoanList:[NSString stringWithFormat:@"%d",self.pageNum]]continueWithBlock:^id(BFTask *task){
        [hud hideAnimated:YES];
        if (task.error) {
            [weakSelf taskToShowError:task.error];
        }else{
            [weakSelf.Loans removeAllObjects];
            PHAPIGetLoanListResponse *loansResponse =(PHAPIGetLoanListResponse *)task.result;
            self.nextPageButton.alpha = 1;
            self.beforePageButton.alpha = 1;
            weakSelf.total =loansResponse.total.integerValue;
            weakSelf.pageNum = loansResponse.page.integerValue;
            for (PHLoan *loan in loansResponse.loanList) {
                [weakSelf.Loans addObject:loan];
            }
            [[[weakSelf taskToGenerateTableModelsFromLoans:weakSelf.Loans]continueWithSuccessBlock:^id(BFTask *task){
                [weakSelf.dataSource removeAllItems];
                [weakSelf.dataSource addItems:task.result];
                return task;
            }] continueWithBlock:^id(BFTask *task) {
                if (task.error) {
                    return [weakSelf taskToShowError:task.error];
                }
                return task;
            }];
        }
        return task;
    }];
}

-(BFTask *)taskToGenerateTableModelsFromLoans:(NSArray<PHLoan *>*)loans{
    return [BFTask taskWithResult:[loans bk_map:^id(id obj) {
        return [[PHTableViewModel alloc] initWithItem:obj cellClass:PHLoanlistTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak PHLoan *item) {
            PHLoanlistTableViewCell *c = (PHLoanlistTableViewCell *)cell;
            [c configureWithLoan:item];
        } collationStringSelector:nil];
    }]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PHTableViewModel *model = [self.dataSource itemAtIndexPath:indexPath];
    typeof(self) __weak weakSelf = self;
    PHLoan *loan = model.item;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[PHObjectManager sharedInstance] taskToGetLoanDetail:[NSString stringWithFormat:@"%d",loan.appId.integerValue]]continueWithBlock:^id(BFTask *task){
        [hud hideAnimated:YES];
        if (task.error) {
            [weakSelf taskToShowError:task.error];
        }else{
            PHLOanListDetailViewController *lendingDetails = [[PHLOanListDetailViewController alloc] initWithLoan:task.result];
            [self.navigationController pushViewController:lendingDetails animated:YES];
        }
        return task;
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"pageNum"]) {
        if(self.pageNum<=1){
            self.beforePageButton.enabled =NO;
        }else{
            self.beforePageButton.enabled =YES;
        }
        if (self.total<self.pageNum*10+1){
            self.nextPageButton.enabled = NO;
        }else{
            self.nextPageButton.enabled = YES;
        }
    }
}
-(void)dealloc{
    [self removeObserver:self forKeyPath:@"pageNum"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
