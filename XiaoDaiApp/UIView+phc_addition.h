//
//  UIView+phc_addition.h
//  p2p
//
//  Created by Philip on 9/7/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (phc_addition)

- (UIView *)phc_configureBottomBorderWithHeight:(CGFloat)height color:(UIColor *)color;

- (UIView *)phc_configureTopBorderWithHeight:(CGFloat)height color:(UIColor *)color;

- (UIView *)phc_configureLeftBorderWithWidth:(CGFloat)width color:(UIColor *)color;

- (UIView *)phc_configureRightBorderWithWidth:(CGFloat)width color:(UIColor *)color;

- (void)phc_setBorderColor:(UIColor *)color;

@end
