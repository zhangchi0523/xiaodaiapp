//
//  PHLagerViewTool.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/10.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHLagerViewTool : UIView
- (instancetype)initWithImage:(UIImage *)image;
-(void)showLagerView;
@end
