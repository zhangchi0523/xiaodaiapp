//
//  PHKeyValueTableViewCell.m
//  p2p
//
//  Created by Philip on 10/23/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHKeyValueTableViewCell.h"
#import "UILabel+phc_addition.h"
#import "UIFont+phc_addition.h"
#import "UIColor+phc_addition.h"
#import <Masonry/Masonry.h>

@interface PHKeyValueTableViewCell()

@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, strong) UIColor *textColor;

@end

@implementation PHKeyValueTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _leftLabel= [UILabel phc_labelWithFontSize:16.0f textColor:_textColor backgroundColor:nil numberOfLines:0];
        _leftLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_leftLabel];

        
        _rightLabel = [UILabel phc_labelWithFontSize:15.0f textColor:_textColor backgroundColor:nil numberOfLines:0];
        _rightLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_rightLabel];
        
        typeof(self) __weak weakSelf = self;
        [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.mas_width).with.multipliedBy(0.3f);
        }];
        
        [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.contentView.mas_right).with.offset(-15.0f);
            make.centerY.equalTo(weakSelf.contentView.mas_centerY);
            make.left.equalTo(_leftLabel.mas_right).with.offset(25.0f);
        }];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.leftLabel.text = nil;
    self.rightLabel.text = nil;
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details {
    [self configureWithTitle:title details:details textColor:[UIColor phc_gray]];
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details textColor:(UIColor *)color{
    [self configureWithTitle:title details:details rightAlignmentMode:NO textColor:color];
}

- (void)configureWithTitle:(NSString *)title details:(NSString *)details rightAlignmentMode:(BOOL)rightAlignmentMode textColor:(UIColor *)color {
    self.leftLabel.text = title;
    self.rightLabel.text = details;
    self.rightLabel.textColor = color;
    
    if (rightAlignmentMode) {
        typeof(self) __weak weakSelf = self;
        [self.leftLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.mas_width).with.multipliedBy(0.25f);
        }];
        
        self.leftLabel.textAlignment = NSTextAlignmentRight;
    }
}
-(void)setTitleTextColor:(UIColor *)color{
    self.leftLabel.textColor = color;
}
-(void)configureWithCellAccessoryType:(UITableViewCellAccessoryType)cellAccessoryType{
    self.accessoryType = cellAccessoryType;

}
@end
