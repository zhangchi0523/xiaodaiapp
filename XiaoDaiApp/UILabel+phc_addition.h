//
//  UILabel+phc_addition.h
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (phc_addition)

+ (instancetype)phc_labelWithFontSize:(CGFloat)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor numberOfLines:(NSUInteger)numberOfLines;

+ (instancetype)phc_labelWithBoldFontSize:(CGFloat)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor numberOfLines:(NSUInteger)numberOfLines;

@end
