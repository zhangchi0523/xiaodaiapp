//
//  UIColor+phc_addition.m
//  p2p
//
//  Created by Philip on 10/16/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "UIColor+phc_addition.h"

@implementation UIColor (phc_addition)

+ (UIColor *)phc_blue {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0x4A90E2);
    });
    
    return color;
}

+ (UIColor *)phc_gray {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0xd8d8d8);
    });
    
    return color;
}

+ (UIColor *)phc_lightGray {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0xf1f1f1);
    });
    
    return color;
}
+ (UIColor *)phc_newGray {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0x999999);
    });
    
    return color;
}

+ (UIColor *)phc_green {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0x329400);
    });
    
    return color;
}

+ (UIColor *)phc_lightBlue {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorFromRGB(0xA5BDD5);
    });
    
    return color;
}

@end
