//
//  PHChangePasswordViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/11.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHChangePasswordViewController.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHTextFieldTableViewCell.h"
#import "UIButton+phc_addition.h"
#import <Masonry/Masonry.h>
#import "PHObjectManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIControl+BlocksKit.h"
#import "PHHeaderTableViewCell.h"
#import <Bolts/Bolts.h>
#import "UIViewController+phc_addition.h"
#import "PHSessionManager.h"

@interface PHChangePasswordViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) NSString *conformPassword;
@property (nonatomic, strong)NSString *password;
@end

@implementation PHChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改密码";
    typeof(self) __weak weakSelf = self;
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"新密码:" placeholder:@"请输入新密码" text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.password = c.textField.text;
                               c.textField.delegate = weakSelf;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHTextFieldTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak id item) {
                           PHTextFieldTableViewCell *c = (PHTextFieldTableViewCell *)cell;
                           [c configureWithTitle:@"确认新密码:" placeholder:@"请确认新密码" text:nil];
                           [c.textField bk_addEventHandler:^(UITextField *textField) {
                               weakSelf.conformPassword = c.textField.text;
                               c.textField.delegate = weakSelf;
                           } forControlEvents:UIControlEventEditingChanged];
                       } collationStringSelector:nil],
                       ];
    [self.dataSource addItems:items];
    [self addObserver:self forKeyPath:@"password" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"conformPassword" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    [self setupFooterView];
    // Do any additional setup after loading the view.
}
- (void)setupFooterView{
    typeof(self) __weak weakSelf = self;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 80.0f)];
    self.nextButton = [UIButton phc_actionButtonWithTitle:@"重置密码"];
    self.nextButton.enabled = NO;
    [footerView addSubview:self.nextButton];
    self.tableView.tableFooterView = footerView;
    
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(footerView);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.9f);
        make.height.equalTo(@40);
    }];
    
    [self.nextButton bk_addEventHandler:^(UIButton *sender) {
        [[[PHObjectManager sharedInstance]taskToChangePassword:weakSelf.password]continueWithBlock:^id(BFTask *task){
            if (task.error) {
                [weakSelf taskToShowError:task.error];
            }else{
                [[weakSelf taskToShowPrompt:@"重置成功"]continueWithBlock:^id(BFTask *task){
                    if ([task.result isEqual:@YES]) {
                        [[PHSessionManager sharedInstance] taskToPerformLogout];
                    }
                    
                    return task;
                    
                }];
            }
            return task;
        }];
    } forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = footerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    self.nextButton.enabled = (self.password.length && self.conformPassword.length);
}
- (void)dealloc {
    [self removeObserver:self forKeyPath:@"password"];
    [self removeObserver:self forKeyPath:@"conformPassword"];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
