//
//  PHLoanTableViewController.h
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewController.h"
extern NSString * const kProvinceListKey;
extern NSString * const kProvinceListID;
extern NSString * const kCityListKey;
extern NSString * const kCityListID;
extern NSString * const kCommunityListKey;
extern NSString * const kCommunityListID;
extern NSString * const kHouseNumberListKey;
extern NSString * const kHouseNumberListI;
extern NSString * const kRoomListKey;
extern NSString * const kRoomListID;
@interface PHLoanTableViewController : PHTableViewController
- (instancetype)initWithProvinceListArray:(NSArray *)provinceListArray withProvinceString:(NSString *)provinceString withCityListArray:(NSArray *)cityListArray withCityString:(NSString *)cityString;
@end
