//
//  PHTopMenuBarViewController.m
//  p2p
//
//  Created by Philip on 9/12/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTopMenuBarViewController.h"
#import "UIButton+phc_addition.h"
#import "UIColor+phc_addition.h"
#import "UIView+phc_addition.h"
#import <Masonry/Masonry.h>
#import <BlocksKit/BlocksKit+UIKit.h>

static const NSUInteger kButtonTagStartIndex = 10000;

@interface PHTopMenuBarViewController()
@property (nonatomic, copy) NSArray<UIViewController *> *controllers;
@property (nonatomic, copy) NSArray<NSString *> *controllerTitles;
@property (nonatomic, strong) UIScrollView *menuScrollview;
@property (nonatomic, assign) NSInteger selectedIndex;
@end

@implementation PHTopMenuBarViewController

- (instancetype)initWithSelectedMenuIndex:(NSUInteger)selectedMenuIndex {
    if (self = [super init]) {
        _selectedIndex = selectedMenuIndex;
    }
    
    return self;
}

- (NSArray<UIViewController *> *)menuViewControllers {
    return nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.controllers = [self menuViewControllers];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.selectedIndex = MIN(self.selectedIndex, self.controllers.count - 1);
    
    self.controllerTitles = [self.controllers bk_map:^id(UIViewController *controller) {
        return controller.tabBarItem.title ? : @"Untitled";
    }];
    
    typeof(self) __weak weakSelf = self;
    
    self.menuScrollview = [UIScrollView new];
    self.menuScrollview.showsHorizontalScrollIndicator = NO;
    self.menuScrollview.backgroundColor = [UIColor phc_lightGray];
    [self.view addSubview:self.menuScrollview];
    
    UIButton *lastButton = nil;
    CGFloat maxIntrinsicHeight = 0.0f;
    BOOL isFixedWidth = self.controllerTitles.count < 5;
    for (NSUInteger index = 0; index < self.controllerTitles.count; index ++) {
        UIButton *button = [UIButton phc_buttonWithFontSize:15.0f title:self.controllerTitles[index] textColor:UIColorFromRGB(0x666666) selectedTextColor:[UIColor phc_blue] titleEdgeInsets:UIEdgeInsetsMake(10.0f, 5.0f, 10.0f, 5.0f)];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.tag = index + kButtonTagStartIndex;
        [button phc_configureBottomBorderWithHeight:3.0f color:UIColorFromRGB(0xbbbbbb)];
        [button addTarget:self action:@selector(menuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.menuScrollview addSubview:button];

        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (lastButton) {
                make.left.equalTo(lastButton.mas_right);
            } else {
                make.left.equalTo(weakSelf.menuScrollview.mas_left);
            }
            make.top.equalTo(weakSelf.menuScrollview.mas_top);
            make.bottom.equalTo(weakSelf.menuScrollview.mas_bottom);
            
            if (isFixedWidth) {
                make.width.equalTo(weakSelf.view.mas_width).with.dividedBy(weakSelf.controllerTitles.count);
            }
        }];
        
        CGSize size = [button systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        maxIntrinsicHeight = MAX(maxIntrinsicHeight, size.height);
        
        lastButton = button;
    }
    
    if (lastButton) {
        [lastButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.menuScrollview.mas_right);
        }];
    }
    
    [self.menuScrollview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.top.equalTo(weakSelf.mas_topLayoutGuide);
        make.height.equalTo(@(maxIntrinsicHeight));
    }];

    [self loadControllerAtIndex:self.selectedIndex];
}

- (void)loadControllerAtIndex:(NSUInteger)index {
    if (self.selectedIndex != NSNotFound) {
        UIViewController *existingController = self.controllers[self.selectedIndex];
        [existingController.view removeFromSuperview];
        [existingController willMoveToParentViewController:nil];
        [existingController removeFromParentViewController];
    }
    
    UIViewController *incomingController = self.controllers[index];
    [self addChildViewController:incomingController];
    [incomingController didMoveToParentViewController:self];
    [self.view addSubview:incomingController.view];
    
    typeof(self) __weak weakSelf = self;
    [incomingController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.menuScrollview.mas_bottom);
        make.bottom.equalTo(weakSelf.mas_bottomLayoutGuide);
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
    }];
    
    self.selectedIndex = index;
    
    for (UIButton *button in self.menuScrollview.subviews) {
        if (![button isKindOfClass:UIButton.class]) continue;
        if (button.tag == (index + kButtonTagStartIndex)) {
            button.selected = YES;
            [button phc_configureBottomBorderWithHeight:3.0f color:[UIColor phc_blue]];
            [self.menuScrollview scrollRectToVisible:button.frame animated:YES];
        } else {
            button.selected = NO;
            [button phc_configureBottomBorderWithHeight:3.0f color:UIColorFromRGB(0xbbbbbb)];
        }
    }
}

- (void)menuButtonTapped:(UIButton *)button {
    NSUInteger index = button.tag - kButtonTagStartIndex;
 
    [self loadControllerAtIndex:index];
}

@end
