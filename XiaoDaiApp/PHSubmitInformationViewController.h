//
//  PHSubmitInformationViewController.h
//  XiaoDaiApp
//
//  Created by 黄瑞东 on 17/3/17.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTableViewController.h"

@interface PHSubmitInformationViewController : PHTableViewController
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *valuationId;

@end
