//
//  PHSearchViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/13.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHSearchViewController.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHTableHeaderView.h"
#import "UIColor+phc_addition.h"
#import "UIViewController+phc_addition.h"
#import <Bolts/Bolts.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import "PHObjectManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIButton+phc_addition.h"
#import "PHTitleTableViewCell.h"
#import "PHObjectManager.h"
#import "PHLoanTableViewController.h"
NSString * const PHNotificationUpdatecommunity = @"PHNotificationUpdatecommunity";
NSString * const PHNotificationUpdatehouse = @"PHNotificationUpdatehouse";

@interface PHSearchViewController ()<UISearchControllerDelegate,UISearchResultsUpdating,UISearchBarDelegate>
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) PHTableHeaderView *headerView;
@property (nonatomic, strong) NSArray *communityListArray;
@property (nonatomic, strong) NSArray *searchArray;
@property (nonatomic, strong) PHObjectManager *objectManager;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation PHSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    typeof(self) __weak weakSelf = self;
    _objectManager = [PHObjectManager sharedInstance];
    _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    _searchController.searchBar.frame = CGRectMake(0, 66, 100, 66);
    _searchController.searchBar.placeholder = @"请输入小区";
    _searchController.searchBar.showsCancelButton = true;

    self.searchController.searchResultsUpdater =self;
    self.searchController.delegate =self;
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;

    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];


    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    if ([self.searchType isEqualToString:@"house"]) {
        [[self taskToGenerateTableModelsFromLoans:self.houseNumberListArray]continueWithSuccessBlock:^id(BFTask *task){
            [self.dataSource removeAllItems];
            [self.dataSource addItems:task.result];
            
            return task;
        }];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    typeof(self) __weak weakSelf = self;
    if ([self.searchType isEqualToString:@"community"]) {
        if (self.cityString) {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[self.objectManager taskToCommunityList:self.cityString communityName:searchText] continueWithBlock:^id(BFTask *task) {
                [weakSelf.hud hideAnimated:YES];
                if (task.error) {
                    [self taskToShowError:task.error];
                }else {
                    self.communityListArray = [NSArray arrayWithArray:task.result];
                    [[weakSelf taskToGenerateTableModelsFromLoans:[self parsingDataListArray:self.communityListArray valueForKey:kCommunityListKey]]continueWithSuccessBlock:^id(BFTask *task){
                        [weakSelf.dataSource removeAllItems];
                        [weakSelf.dataSource addItems:task.result];
                        
                        return task;
                    }];
                }
                return task;
            }];
        }
    }
    else{
        NSPredicate* pred = [NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@" ,searchText];
        _searchArray = [self.houseNumberListArray filteredArrayUsingPredicate:pred];
        [[self taskToGenerateTableModelsFromLoans:_searchArray]continueWithSuccessBlock:^id(BFTask *task){
            [weakSelf.dataSource removeAllItems];
            [weakSelf.dataSource addItems:task.result];
            
            return task;
        }];
    }

}
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.searchType isEqualToString:@"community"]) {
        PHTableViewModel *model = [self.dataSource itemAtIndexPath:indexPath];
        NSString *str  = model.item;
        NSDictionary *dic = @{@"communityName":str,@"communityListArray":self.communityListArray};
        [[NSNotificationCenter defaultCenter]postNotificationName:PHNotificationUpdatecommunity object:nil userInfo:dic];
        [self.navigationController popViewControllerAnimated:YES];

    }else{
        PHTableViewModel *model = [self.dataSource itemAtIndexPath:indexPath];
        NSString *str  = model.item;
        NSDictionary *dic = @{@"houseName":str};
        [[NSNotificationCenter defaultCenter]postNotificationName:PHNotificationUpdatehouse object:nil userInfo:dic];
        [self.navigationController popViewControllerAnimated:YES];

    }
    
}
-(BFTask *)taskToGenerateTableModelsFromLoans:(NSArray*)array{
    return [BFTask taskWithResult:[array bk_map:^id(id obj) {
        return [[PHTableViewModel alloc] initWithItem:obj cellClass:PHTitleTableViewCell.class configurationBlock:^(UITableViewCell *cell, __weak NSString * item) {
            PHTitleTableViewCell *c = (PHTitleTableViewCell *)cell;
            [c configureWithTitle:item textColor:[UIColor blackColor]];

        } collationStringSelector:nil];
    }]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSString *)traverseAttributeDataArray:(NSArray *)array name:(NSString *)name nameKey:(NSString *)nameKey IDKey:(NSString *)IDKey {
    for (int i = 0; i < array.count; i++) {
        if ([[[array objectAtIndex:i]valueForKey:nameKey] isEqualToString:name]) {
            return [[array objectAtIndex:i]valueForKey:IDKey];
        }
    }
    return nil;
}

- (NSArray *)parsingDataListArray:(NSArray *)array valueForKey:(NSString *)key{
    
    NSMutableArray *mutableArray = [NSMutableArray new];
    for (int i = 0; i < array.count; i++) {
        [mutableArray addObject:[[array objectAtIndex:i] valueForKey:key]];
    }
    return mutableArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
