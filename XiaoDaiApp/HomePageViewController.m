//
//  HomePageViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/15.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "HomePageViewController.h"
#import <Masonry.h>
#import "PHLoanTableViewController.h"
#import "PHUpdatePictureViewController.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <Bolts/Bolts.h>
#import "PHObjectManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PHAPIValuationResponse.h"
#import "UIViewController+phc_addition.h"
@interface HomePageViewController ()

@property(nonatomic,strong)PHLoanTableViewController *loanViewController;

@end

@implementation HomePageViewController

- (instancetype)init {
    if (self = [super init]) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"home_icon"] selectedImage:nil];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(5.0f, 0.0f, -5.0f, 0.0f);

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

-(void)setUI {
    UIImageView *backgroundPage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"homePage"]];
    backgroundPage.userInteractionEnabled = YES;
    typeof(self) __weak weakSelf = self;
    [self.view addSubview:backgroundPage];
    UITapGestureRecognizer *panRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickHouseButton)];
    [backgroundPage addGestureRecognizer:panRecognizer];
    [backgroundPage mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(weakSelf.view.mas_top);
        make.bottom.equalTo(weakSelf.view.mas_bottom);
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
    }];
}

-(void)clickHouseButton{
    typeof(self) __weak weakSelf = self;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[PHObjectManager sharedInstance] taskToProvincesList] continueWithBlock:^id(BFTask *task) {
        [hud hideAnimated:YES];
        if (task.error) {
            [weakSelf taskToShowError:task.error];
        }else {
            NSArray *provinceListArray = [NSArray arrayWithArray:task.result];
            NSString *provinceFirstString = [[provinceListArray objectAtIndex:0] objectForKey:kProvinceListKey];
            NSString *cityString =[[provinceListArray objectAtIndex:0] objectForKey:kProvinceListID];
            [[[PHObjectManager sharedInstance] taskToCityList:cityString] continueWithBlock:^id(BFTask *task) {
                [hud hideAnimated:YES];
                if (task.error) {
                    [weakSelf taskToShowError:task.error];
                }else {
                    NSArray *cityListArray = [NSArray arrayWithArray:task.result];
                    NSString *cityfirstString =[[cityListArray objectAtIndex:0] objectForKey:kCityListKey];
                    weakSelf.loanViewController =[[PHLoanTableViewController alloc]initWithProvinceListArray:provinceListArray withProvinceString:provinceFirstString withCityListArray:cityListArray withCityString:cityfirstString];
                    [self.navigationController pushViewController:weakSelf.loanViewController animated:YES];

                }
                
                
                return task;
            }];
        }
        return task;
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

@end
