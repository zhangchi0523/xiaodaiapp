//
//  PHPersonalCenterViewController.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/7.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHPersonalCenterViewController.h"
#import "PHTableViewDataSource.h"
#import "PHTableViewModel.h"
#import "PHHeaderTableViewCell.h"
#import "PHMenuTableViewCell.h"
#import <Masonry.h>
#import "UIColor+phc_addition.h"
#import "UIImage+phc_addition.h"
#import "UILabel+phc_addition.h"
#import "UIButton+phc_addition.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "PHLoanListViewController.h"
#import "PHSessionManager.h"
#import "PHUserProfile.h"
#import "PHReferralQRCodeViewController.h"
#import "PHObjectManager.h"
#import <Bolts/Bolts.h>
#import <AFNetworking/AFNetworking.h>
#import "PHChangePasswordViewController.h"
#import "UIViewController+phc_addition.h"
@interface PHPersonalCenterViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) PHTableViewDataSource *dataSource;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *branchLabel;
@property (nonatomic, strong) NSString *branchName;

@end

@implementation PHPersonalCenterViewController

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"我的";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"personalCenter_icon"] selectedImage:nil];
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(5.0f, 0.0f, -5.0f, 0.0f);    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor phc_lightGray];
    self.tableView.bounces = NO;
    self.dataSource = [[PHTableViewDataSource alloc] initWithTableView:self.tableView showIndexTitle:NO];
    NSArray *items = @[
                       [[PHTableViewModel alloc] initWithItem:nil cellClass:PHHeaderTableViewCell.class configurationBlock:nil collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"修改密码" details:nil withLeftImage:[UIImage imageNamed:@"center_password"]];
                       }collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"身份认证" details:nil withLeftImage:[UIImage imageNamed:@"center_identity"]];
                       }collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"申请查询" details:nil withLeftImage:[UIImage imageNamed:@"center_manager"]];
                       }collationStringSelector:nil],
                       [[PHTableViewModel alloc]initWithItem:nil cellClass:PHMenuTableViewCell.class configurationBlock:^(UITableViewCell *cell,__weak id item){
                           PHMenuTableViewCell *c = (PHMenuTableViewCell *)cell;
                           [c configureWithTitle:@"我的二维码" details:nil withLeftImage:[UIImage imageNamed:@"center_qrcode"]];
                       }collationStringSelector:nil]
                    ];
    [self.dataSource addItems:items];
    [self setUpHeaderView];
    [self setUpFooterView];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:NO];

}
-(void)viewDidDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setTranslucent:YES];

}
-(void)setUpHeaderView{
    typeof(self) __weak weakSelf = self;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(-1.0f, 0.0f, 0.0f,150.0f)];
    headerView.backgroundColor = [UIColor phc_blue];
    NSString *filePath =[self headerImagePath:PHImageName];
    if ([UIImage imageWithContentsOfFile:filePath]){
        _headImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:filePath]];

    }else{
        _headImageView = [[UIImageView alloc] initWithImage:[UIImage phc_imageWithColor:[UIColor grayColor]]];

    }
    
    self.headImageView.userInteractionEnabled = YES;
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.layer.cornerRadius = 40;
    [headerView addSubview:self.headImageView];
    PHSessionManager *sessicon = [[PHSessionManager alloc]init];
    _branchLabel = [UILabel phc_labelWithBoldFontSize:16.0f textColor:[UIColor whiteColor] backgroundColor:nil numberOfLines:0];
    self.branchLabel.text = [NSString stringWithFormat:@"渠道名称:%@",[[sessicon.currentUserProfile.branch objectForKey:@"name"] stringByRemovingPercentEncoding]];
    self.branchLabel.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:self.branchLabel];
    
    self.tableView.tableHeaderView = headerView;
    [self.headImageView bk_whenTapped:^{
        [weakSelf choseAndTakePicture];
    }];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.centerX.equalTo(headerView.mas_centerX);
        make.centerY.equalTo(headerView.mas_centerY).with.offset(-10.0f);
        make.width.equalTo(@(80.0f));
        make.height.equalTo(@(80.0f));
    }];
    [self.branchLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.centerX.equalTo(headerView.mas_centerX);
        make.top.equalTo(weakSelf.headImageView.mas_bottom).with.offset(5.0f);
        make.width.equalTo(@(300.0f));
        make.height.equalTo(@(16.0f));
    }];
}
-(void)setUpFooterView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f,150.0f)];
    UIButton *logoutButton = [UIButton phc_buttonWithFontSize:16.0f title:@"退出" textColor:[UIColor whiteColor] backgroundImage:[UIImage phc_imageWithColor:[UIColor phc_blue]] selectedBackgroundImage:nil];
    logoutButton.layer.cornerRadius = 5.0f;
    logoutButton.clipsToBounds = YES;
    [footerView addSubview:logoutButton];
    self.tableView.tableFooterView = footerView;
    [logoutButton bk_whenTapped:^{
        [[PHSessionManager sharedInstance] taskToPerformLogout];
    }];
    [logoutButton mas_makeConstraints:^(MASConstraintMaker *make){
        make.centerX.equalTo(footerView.mas_centerX);
        make.centerY.equalTo(footerView.mas_centerY);
        make.width.equalTo(footerView.mas_width).with.multipliedBy(0.9f);
        make.height.equalTo(@(40.0f));
    }];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 1:{
            [self.navigationController pushViewController:[PHChangePasswordViewController new] animated:YES];
        }
            break;
        case 3:{
            [self.navigationController pushViewController:[PHLoanListViewController new] animated:YES];
        }
            break;
        case 4:{
            [self.navigationController pushViewController:[PHReferralQRCodeViewController new] animated:NO];
        }
            break;
            
        default:
            break;
    }
}
-(void)choseAndTakePicture{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = NO;
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePicker animated:YES completion:nil];
        } else {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    UIAlertAction *pictureAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:photoAction];
    [alertController addAction:pictureAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *editedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [editedImage phc_imageToReszieWithsize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height) compressionQuality:0.5f];
        [self.headImageView setImage:image];
        NSData *data = UIImageJPEGRepresentation(image, 1);

        NSString *pathFile = [self headerImagePath:PHImageName];
        [data writeToFile:pathFile atomically:NO];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
