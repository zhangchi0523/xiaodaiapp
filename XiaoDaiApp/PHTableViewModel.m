//
//  PHTableViewModel.m
//  p2p
//
//  Created by Philip on 9/8/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTableViewModel.h"

@interface PHTableViewModel()
@property (nonatomic, readwrite, strong) id item;
@property (nonatomic, readwrite, copy) Class cellClass;
@property (nonatomic, readwrite, assign) SEL collationStringSelector;
@property (nonatomic, readwrite, copy) void (^configurationBlock)(UITableViewCell *cell, __weak id item);
@end

@implementation PHTableViewModel

- (instancetype)initWithItem:(id)item cellClass:(Class)cellClass configurationBlock:(void(^)(UITableViewCell *cell, __weak id item))configurationBlock collationStringSelector:(SEL)collationStringSelector {
    if (self = [super init]) {
        _item = item;
        _cellClass = [cellClass copy];
        _configurationBlock = configurationBlock;
        _collationStringSelector = collationStringSelector;
    }
    
    return self;
}

- (void)updateItem:(id)newItem {
    self.item = newItem;
}

@end
