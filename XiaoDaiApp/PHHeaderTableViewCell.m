//
//  PHHeaderTableViewCell.m
//  p2p
//
//  Created by Philip on 10/21/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHHeaderTableViewCell.h"
#import "UILabel+phc_addition.h"
#import "UIImage+phc_addition.h"
#import "UIColor+phc_addition.h"
#import <Masonry/Masonry.h>

@interface PHHeaderTableViewCell()
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation PHHeaderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor phc_lightGray];
        
        _titleLabel = [UILabel phc_labelWithFontSize:10.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:0];
        [_titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        [_titleLabel setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        _titleLabel.text = @" ";
        [self.contentView addSubview:_titleLabel];
        
        typeof(self) __weak weakSelf = self;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
        }];
    }
    
    return self;
}

@end
