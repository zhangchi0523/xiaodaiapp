//
//  PHTableHeaderView.m
//  p2p
//
//  Created by Philip on 9/15/15.
//  Copyright © 2015 PHSoft. All rights reserved.
//

#import "PHTableHeaderView.h"
#import "UILabel+phc_addition.h"
#import "UIColor+phc_addition.h"
#import "UIView+phc_addition.h"
#import <Masonry/Masonry.h>


@interface PHTableHeaderView()
@property (nonatomic, copy) NSArray *titles;
@end

@implementation PHTableHeaderView

- (instancetype)initWithTitles:(NSArray<NSString *> *)titles withInsets:(UIEdgeInsets)edgeInsets {
    if (self = [super init]) {
        self.backgroundColor = [UIColor phc_lightGray];
        [self phc_configureBottomBorderWithHeight:1.0f color:[UIColor phc_gray]];
        
        _titles = [titles copy];

        NSUInteger size = _titles.count;
        UILabel *lastLabel = nil;

        typeof(self) __weak weakSelf = self;
        for (NSUInteger index = 0; index < size; index ++) {
            UILabel *label = [UILabel phc_labelWithFontSize:13.0f textColor:[UIColor darkGrayColor] backgroundColor:nil numberOfLines:0];
            label.text = _titles[index];
            label.textAlignment = NSTextAlignmentCenter;
            [self addSubview:label];
            
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.mas_top).with.offset(edgeInsets.top);
                make.bottom.equalTo(weakSelf.mas_bottom).with.offset(-edgeInsets.bottom);
                
                if (lastLabel) {
                    make.left.equalTo(lastLabel.mas_right).with.priorityHigh();
                    make.width.equalTo(lastLabel.mas_width);
                } else {
                    make.left.equalTo(weakSelf.mas_left).with.offset(edgeInsets.left);
                }
            }];
            
            lastLabel = label;
        }
        
        if (lastLabel) {
            [lastLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(weakSelf.mas_right).with.offset(-edgeInsets.right);
            }];
        }
    }
    
    return self;
}

@end
