//
//  PHTitleTableViewCell.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/3/31.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "PHTitleTableViewCell.h"
#import "UILabel+phc_addition.h"
#import "UIFont+phc_addition.h"
#import "UIColor+phc_addition.h"
#import <Masonry/Masonry.h>
@interface PHTitleTableViewCell()
@property (nonatomic,readwrite,strong) UILabel *titleLabel;
@end

@implementation PHTitleTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _titleLabel= [UILabel phc_labelWithBoldFontSize:16.0f textColor:[UIColor blackColor] backgroundColor:nil numberOfLines:0];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_titleLabel];
        typeof(self) __weak weakSelf = self;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.contentView.mas_top).with.offset(10.0f);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-10.0f);
            make.left.equalTo(weakSelf.contentView.mas_left).with.offset(10.0f);
            make.width.equalTo(weakSelf.mas_width).with.multipliedBy(0.5f);
        }];
        
    }
    
    return self;

}
- (void)configureWithTitle:(NSString *)title textColor:(UIColor *)color{
    self.titleLabel.text = title;
    self.titleLabel.textColor = color;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
