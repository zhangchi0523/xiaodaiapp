//
//  NSNumber+phc_addition.m
//  XiaoDaiApp
//
//  Created by 张驰 on 2017/4/10.
//  Copyright © 2017年 张驰. All rights reserved.
//

#import "NSNumber+phc_addition.h"

@implementation NSNumber (phc_addition)
- (NSNumberFormatter *)currencyFormatter {
    static NSNumberFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.groupingSeparator = @",";
        formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
    });
    
    return formatter;
}
-(NSString *)stringByTrimming:(NSString *)str{
    NSString *subStr = [str substringWithRange:NSMakeRange(0, 1)];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:subStr];
    return [str stringByTrimmingCharactersInSet:set];
}
-(NSString *)currencyString{
    return [self stringByTrimming:[[self currencyFormatter] stringFromNumber:[[NSNumber alloc]initWithDouble:self.doubleValue]]];
}
@end
