//
//  PHUserProfile.h
//  p2p
//
//  Created by trila on 15/10/9.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import "Mantle.h"

@interface PHUserProfile : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *token;
@property (nonatomic, readonly, copy) NSString *agents;
@property (nonatomic, readonly, copy) NSDictionary *branch;
@property (nonatomic, readonly, copy) NSString *phone;
@property (nonatomic, readonly, copy) NSString *role;
@property (nonatomic, readonly, copy) NSString *identificationNumber;
@property (nonatomic, readonly, copy) NSString *supervisor;


@end
