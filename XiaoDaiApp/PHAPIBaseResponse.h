//
//  APIResponse.h
//  p2p
//
//  Created by trila on 15/10/12.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import <Mantle.h>

@interface PHAPIBaseResponse : MTLModel<MTLJSONSerializing>

@property(nonatomic, readonly, copy) NSURL* request;
@property(nonatomic, readonly, copy) NSString* message;
@property(nonatomic, readonly, copy) NSDictionary* data;
@property(nonatomic, readonly, assign) NSInteger errorCode;
@property(nonatomic, readonly, assign) NSInteger code;

@end
