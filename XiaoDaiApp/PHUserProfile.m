//
//  PHUserProfile.m
//  p2p
//
//  Created by trila on 15/10/9.
//  Copyright © 2015年 PHSoft. All rights reserved.
//

#import "PHUserProfile.h"

@interface PHUserProfile ()

@property (nonatomic, readwrite, copy) NSString *name;
@property (nonatomic, readwrite, copy) NSString *token;
@property (nonatomic, readwrite, copy) NSString *agents;
@property (nonatomic, readwrite, copy) NSDictionary *branch;
@property (nonatomic, readwrite, copy) NSString *phone;
@property (nonatomic, readwrite, copy) NSString *role;
@property (nonatomic, readwrite, copy) NSString *identificationNumber;
@property (nonatomic, readwrite, copy) NSString *supervisor;

@end


@implementation PHUserProfile

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"name":@"name",
             @"token":@"token",
             @"agents":@"agents",
             @"branch":@"branch",
             @"phone":@"phone",
             @"role":@"role",
             @"identificationNumber":@"identificationNumber",
             @"supervisor":@"supervisor",
             };
}

+ (NSValueTransformer *)verifiedJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)isCreditDownJSONTransformer {
    return [MTLValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
